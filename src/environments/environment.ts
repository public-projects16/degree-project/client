// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { LogLevel } from 'msal';

export function loggerCallback(logLevel: LogLevel.Verbose, message, piiEnabled: true) {}


export const environment = {
  production: false,
  API: 'http://localhost:3000',
  ANGULAR_API: 'http://localhost:4200',
  tokenKey: 'msal.idtoken',
  APPINSIGHTS_INSTRUMENTATIONKEY: 'be207b5c-2b71-4103-b016-e586cf063504',
  tenant: 'Helix123Auth.onmicrosoft.com',
  clientID: 'be207b5c-2b71-4103-b016-e586cf063504',
  signInPolicy: 'B2C_1_all',
  redirectUri: 'http://localhost:4200/',
  b2cScopes: ['openid', 'https://Helix123Auth.onmicrosoft.com/helix-auth/user_impersonation'],
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
