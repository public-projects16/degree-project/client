import { NgModule } from '@angular/core';
import {
  MatTableModule,
  MatPaginatorModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatInputModule,
  MatFormFieldModule,
  MatRadioModule,
  MatSelectModule,
  MatChipsModule,
  MatAutocompleteModule,
  MatSortModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatToolbarModule,
  MatSidenavModule,
  MatStepperModule,
  MatListModule,
  MatProgressBarModule,
  MatExpansionModule,
} from '@angular/material';

@NgModule({
  declarations: [],
  imports: [],
  exports: [
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatSortModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatSelectModule,
    MatToolbarModule,
    MatStepperModule,
    MatSidenavModule,
    MatListModule,
    MatProgressBarModule,
    MatExpansionModule
  ]
})
export class MaterialModule { }
