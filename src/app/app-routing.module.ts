import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './authGuard';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { CostumerComponent } from './pages/costumer/costumer.component';
import { WorkerComponent } from './pages/worker/worker.component';
import { MerchantComponent } from './pages/merchant/merchant.component';
import { ShowAccountComponent } from './pages/show-account/show-account.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { CompanyDataComponent } from './pages/company-data/company-data.component';
import { RegisterCompanyComponent } from './pages/register-company/register-company.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { ProductsComponent } from './pages/products/products.component';
import { CandidatesComponent } from './pages/candidates/candidates.component';
import { ProductsToSellComponent } from './pages/products-to-sell/products-to-sell.component';
import { DisabledAccountComponent } from './pages/disabled-account/disabled-account.component';
import { DisabledCompanyComponent } from './pages/disabled-company/disabled-company.component';
import { CompanyTransactionsComponent } from './pages/company-transactions/company-transactions.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard]},
  { path: 'costumer', component: CostumerComponent, canActivate: [AuthGuard]},
  { path: 'worker', component: WorkerComponent, canActivate: [AuthGuard]},
  { path: 'merchant', component: MerchantComponent, canActivate: [AuthGuard]},
  {path: 'account', component: ShowAccountComponent, canActivate: [AuthGuard]},
  {path: 'transactions', component: TransactionsComponent, canActivate: [AuthGuard]},
  {path: 'company_data', component: CompanyDataComponent, canActivate: [AuthGuard]},
  {path: 'register-company', component: RegisterCompanyComponent, canActivate: [AuthGuard]},
  {path: 'companies', component: CompaniesComponent, canActivate: [AuthGuard]},
  {path: 'candidates', component: CandidatesComponent, canActivate: [AuthGuard]},
  {path: 'products', component: ProductsComponent, canActivate: [AuthGuard]},
  {path: 'companyProducts', component: ProductsToSellComponent, canActivate: [ AuthGuard]},
  {path: 'disabled/company', component: DisabledCompanyComponent, canActivate: [AuthGuard]},
  {path: 'company/transactions', component: CompanyTransactionsComponent, canActivate: [AuthGuard]},
  {path: 'disabled', component: DisabledAccountComponent},
  { path: 'test', component: DisabledAccountComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
