import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../services/http/http-service.service';
import { Links } from '../Utils/Links';
import { IResponse } from '../interfaces/IResponse';
import { Router } from '@angular/router';
import { Roles } from '../Utils/Constants';

import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public links = Links;

  constructor(
    private http: HttpServiceService,
    private router: Router) { }

  /**
   * @returns void
   */
  public ngOnInit() {
    if (!_.isUndefined(this.http)) {
      this.http.validateUserRegistered().subscribe((data: IResponse) => {
        if (data.status === 'error') {
          this.router.navigate(['/register']);
        } else {
          if (data.data.info[0] === Roles.costumer) {
            this.router.navigate(['/costumer']);
          } else if (data.data.info[0] === Roles.worker) {
            this.router.navigate(['/worker']);
          } else {
            this.router.navigate(['/merchant']);
          }
        }
      });
    }
  }

}
