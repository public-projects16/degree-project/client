export const costumerTable = {
    ids: ['company', 'buys', 'last'],
    headers: { company: 'Empresa', buys: 'No. de compras', last: 'Última compra'}
};

export const costumerTransactionsTable = {
    ids: ['company', 'amount', 'concept', 'date'],
    headers: {company: 'Empresa', amount: 'Cantidad', concept: 'Concepto', date: 'Fecha de transacción'}
};

export const companiesOnCollegesTable = {
    ids: ['company', 'actions'],
    headers: {company: 'Empresa', actions: 'Visualizar'}
};

export const productTable = {
    ids: ['name', 'amount', 'cost', 'actions', 'edit', 'remove'],
    headers: {name: 'Producto', amount: 'Cantidad', cost: 'Costo', actions: 'Ver detalle', edit: 'Editar', remove: 'Eliminar'}
};

export const workersTable = {
    ids: ['name', 'email', 'accept', 'remove'],
    headers: {name: 'Nombre', email: 'Correo',  accept: 'Aceptar', remove: 'Declinar'}
};

export const productsToSell = {
    ids: ['name', 'price', 'amount', 'subtotal'],
    headers: {name: 'Producto', price: 'Precio', amount: 'Cantidad', subtotal: 'Subtotal'}
};

export const companyTransactions = {
    ids: ['concept', 'amount', 'date'],
    headers: {concept: 'Concepto', amount: 'Total', date: 'Fecha'}
};
