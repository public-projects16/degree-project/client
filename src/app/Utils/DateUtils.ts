export class DateUtils {

    /**
     * @description Returns a standard date format to show in the tables
     * @param Date date
     * @returns string
     */
    public static formatDate(date: Date): string {
        let dateString = '';
        dateString = date.getDate().toString() + ' - ' + date.getMonth().toString() + ' - ' + date.getFullYear().toString();
        dateString +=  '  ' + date.getHours().toString() + ':' + date.getMinutes();
        return dateString;
      }
}
