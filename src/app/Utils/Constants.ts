export enum CONSTANTS {
    INITIAL_ID = -1,
    ZERO = 0,
    ONE = 1,
    FOUR = 4,
    SUCCESS = 'success',
    ERROR = 'error',
    COMMERCIAL_INFO = 'commercialInfo',
    PRODUCT = 'product',
    ONE_SECOND = 1000,
    ONE_HUNDRED = 100,
}

export const Roles = {
    merchant: 'merchant',
    costumer: 'costumer',
    worker: 'worker'
};

export const UserStatus = {
    available: 1,
    disabled: 2,
    waiting: 3,
    rejected: 4
};

export const CompanyDataNames = {
    id: 'id',
    typeOfTaxpayer: 'typeOfTaxpayer',
    commercialInfo: 'commercialInfo',
    taxData: 'taxData',
    legalDocumentation: 'legalDocumentation',
    bankInformation: 'bankInformation'
};

export const TaxDataNames = {
    id: 'id',
    phone: 'phone',
    taxAddress: 'taxAddress',
    taxRfc: 'taxRfc',
    typeOfTaxpayer: 'typeOfTaxpayer'
};

export const BankInformationNames = {
    id: 'id',
    secretKey: 'secretKey',
    openpayId: 'openpayId',
    clabe: 'clabe'
};

export const CommercialInfoNames = {
    id : 'id',
    name : 'name',
    webUrl : 'webUrl',
    contactMail : 'contactMail',
    contactPhone : 'contactPhone',
    socialNetwork : 'socialNetwork',
    commercialBusiness : 'commercialBusiness'
};

export const LegalDocumentsName = {
    id: 'id',
    constitutiveAct: 'constitutiveAct',
    cfid: 'cfid',
    ownerId: 'ownerId',
    proofOfTaxAddress: 'proofOfTaxAddress',
};

export const ProductsNames = {
    id: 'id',
    name: 'name',
    cost: 'cost',
    image: 'image',
    amount: 'amount',
    description: 'description'
};
