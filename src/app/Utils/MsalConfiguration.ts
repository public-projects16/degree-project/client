import { LogLevel, Logger  } from 'msal';
import { FrameworkOptions, Configuration, AuthOptions, CacheOptions, SystemOptions } from 'msal/lib-commonjs/Configuration';
import { environment } from 'src/environments/environment';

export function loggerCallback(logLevel: LogLevel.Verbose, message , piiEnabled: true) {}


class MsalConfiguration {

  private resourceMap: any;
  private configuration: Configuration;

  constructor() {
    this.setResourceMap();
    this.setConfiguration();
  }

  /**
   * @param LogLevel.Verbose logLevel The log level
   * @param string message The message thas is going to be sended
   * @param true piiEnabled The pii
   */
  public loggerCallback(logLevel: LogLevel.Verbose, message: string, piiEnabled: true) { }

  /**
   * @returns
   * @memberof MsalConfiguration
   */
  public getConfiguration() {
    return this.configuration;
  }

  /**
   * @private
   * @memberof MsalConfiguration
   */
  private setResourceMap(): void {
    this.resourceMap =  [ environment.b2cScopes ];
  }

  /**
   * @private
   * @returns {AuthOptions}
   * @memberof MsalConfiguration
   */
  private getAuthOptions(): AuthOptions {
    return {
      clientId:  environment.clientID,
      authority: 'https://Helix123Auth.b2clogin.com/tfp/' + environment.tenant +  '/' + environment.signInPolicy,
      validateAuthority: false,
      redirectUri: 'http://localhost:4200/',
      postLogoutRedirectUri:  'http://localhost:4200/',
      navigateToLoginRequestUrl: false,
    };
  }

  /**
   * @private
   * @returns {CacheOptions}
   * @memberof MsalConfiguration
   */
  private getCacheOptions(): CacheOptions {
    return {
      cacheLocation: 'localStorage',
    };
  }

  /**
   * @private
   * @returns {SystemOptions}
   * @memberof MsalConfiguration
   */
  private getSystemOptions(): SystemOptions {
    return {
      logger: new Logger( this.loggerCallback , {
        level: LogLevel.Info,
        correlationId: '03da1969-a5dc-45ef-bcc3-55283ea47a7f',
        piiLoggingEnabled: true,
      }),
    };
  }

  /**
   * @private
   * @returns {FrameworkOptions}
   * @memberof MsalConfiguration
   */
  private getFrameworkOptions(): FrameworkOptions {
    return {
      isAngular: true,
      unprotectedResources:  ['https://www.microsoft.com/en-us/'],
      protectedResourceMap: this.resourceMap
    };
  }

  /**
   * @private
   * @memberof MsalConfiguration
   */
  private setConfiguration() {
    this.configuration = {
      auth: this.getAuthOptions(),
      cache: this.getCacheOptions(),
      system: this.getSystemOptions(),
      framework: this.getFrameworkOptions()
    };
  }
}


const msalConfiguration = new MsalConfiguration();
export default msalConfiguration;
