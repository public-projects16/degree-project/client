export const Links = {
    logo: 'https://firebasestorage.googleapis.com/v0/b/escomonedas.appspot.com' +
    '/o/helix%2FhelixLogoClaro.png?alt=media&token=4dec5d66-2066-4ee8-b951-1643474c23e8',
    curp: 'https://www.gob.mx/curp/',
    productsTemplate: 'https://firebasestorage.googleapis.com/v0/b/escomonedas.' +
    'appspot.com/o/helix%2FPlantilla%20de%20productos.xlsx?alt=media&token=414d90b1-1631-4c2b-abae-a5fe113177a6',
    logo_gif: 'https://firebasestorage.googleapis.com/v0/b' +
    '/escomonedas.appspot.com/o/helix%2Fhelix.gif?alt=media&token=decd865d-061b-48bf-9b0a-e5e04f8f50eb'
};
