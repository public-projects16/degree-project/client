import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { IResponse } from 'src/app/interfaces/IResponse';
import { Roles } from 'src/app/Utils/Constants';
import { costumerTransactionsTable } from 'src/app/Utils/TableHeaders';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { IUser  } from 'src/app/interfaces/ITables';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import * as _ from 'lodash';
import { DateUtils } from 'src/app/Utils/DateUtils';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  private userData: IUser;
  private ids: Array<string>;
  private headers: any;
  private tableData: Array<any>;
  private showTable: boolean;

  constructor(
    private http: HttpServiceService,
    private notification: NotificationsService,
    private storage: LocalStorageServiceService
  ) {
    this.tableData = new Array<any>();
    this.showTable = false;
  }

  ngOnInit() {
    if (!_.isUndefined(this.storage)) {
      this.storage.getUserInfo().then((user: IUser) => {
        this.userData = user;
        this.http.getCostumerTransactions(this.userData.id).subscribe((response: IResponse) => {
          if (response.status === 'success') {
            this.setTableData(response.data.info);
          } else  { this.notification.error(response.error.info, 'Error'); }
        });
      })
      .catch((error) => {
        this.notification.error(error, 'Error');
      });
    }
  }

  setTableData(transactions: any[]) {
    if (this.userData.type === Roles.costumer) {
      _.forEach(transactions, (currentTransaction) => {
        const newDate = new Date(currentTransaction._date);
        this.tableData.push({
          company: currentTransaction._company.commercialInfo.name,
          amount: currentTransaction._shoppingTotal,
          concept: currentTransaction._concept,
          date: DateUtils.formatDate(newDate)
        });
      });
      this.ids = costumerTransactionsTable.ids;
      this.headers = costumerTransactionsTable.headers;
      this.showTable = true;
    }
  }
}
