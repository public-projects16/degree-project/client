import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import { TransactionsComponent } from './transactions.component';

describe('TransactionsComponent', () => {
  let component: TransactionsComponent;
  let fixture: ComponentFixture<TransactionsComponent>;
  let http: HttpServiceService;
  let notification: NotificationsService;
  let storage: LocalStorageServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsComponent ],
      providers: [
        { provide: HttpServiceService, useValue: http },
        { provide: NotificationsService, useValue: notification },
        { provide: LocalStorageServiceService, useValue: storage }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
