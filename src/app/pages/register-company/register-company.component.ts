import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Links } from 'src/app/Utils/Links';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Company } from 'src/app/clases/company';
import { TaxDataNames, CONSTANTS, LegalDocumentsName } from 'src/app/Utils/Constants';
import { BankInformation } from 'src/app/clases/BankInformation';
import { TaxData } from 'src/app/clases/TaxData';
import { CommercialInfo } from 'src/app/clases/CommercialInfo';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { IResponse } from 'src/app/interfaces/IResponse';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { IUploaderData } from 'src/app/interfaces/IComponents';
import { College } from 'src/app/clases/College';

import * as _ from 'lodash';
import { UploadFilesComponent } from 'src/app/components/upload-files/upload-files.component';

@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.css']
})
export class RegisterCompanyComponent implements OnInit {

  @ViewChild('nextStepDocuments', {static: false}) nextStepDocuments: ElementRef<HTMLElement>;
  @ViewChild('upload', {static: false}) uploader: UploadFilesComponent;
  private links = Links;
  private userId: number;
  private taxPlayerType: string;
  private CommercialInfoForm: FormGroup;
  private TaxDataForm: FormGroup;
  private BankInfoForm: FormGroup;
  private documentFiles: Array<IUploaderData>;
  private imageNames: Array<IUploaderData>;
  private filesToUpdate: FormData;
  private companyLogo: FormData;
  private step: number;
  private currentCollege: College;
  private sumbitAll: boolean;
  private uploadFiles: boolean;
  private showImageUpload: boolean;

  constructor(
    private router: ActivatedRoute,
    private appRouter: Router,
    private formBuilder: FormBuilder,
    private http: HttpServiceService,
    private notifications: NotificationsService
  ) {
     this.step = CONSTANTS.ZERO;
     this.sumbitAll = false;
     this.uploadFiles = false;
     this.showImageUpload = false;
     if (!_.isUndefined(this.router)) {
      this.router.params.subscribe(params => { this.userId = params.userId; });
    }
  }

  /**
   * @description Acts when the component init
   * @return void
   */
  public ngOnInit(): void {
    this.uploadFiles = false;
    this.generateForms();
    this.setDocumentsHeaders();
  }

  /**
   * @description Generate the forms which are going to be used
   * @return void
   */
  public generateForms(): void {
    this.CommercialInfoForm = this.formBuilder.group({
      name: ['', Validators.required],
      webUrl: ['', Validators.required],
      contactMail: ['', Validators.required],
      contactPhone: ['', Validators.required],
      socialNetwork: [''],
      commercialBusiness: ['', Validators.required]
    });

    this.TaxDataForm = this.formBuilder.group({
      taxRfc: ['', Validators.required],
      phone: ['', Validators.required],
      taxAddress: ['', Validators.required]
    });

    this.BankInfoForm = this.formBuilder.group({
      openpayId: ['', Validators.required],
      secretKey: ['', Validators.required],
      clabe: ['', Validators.required]
    });
  }

  /**
   * @description Register the company
   * @return void
   */
  public RegisterCompanyData() {
   const companyData: Company = this.GenerateCompanyData();
   const requestData = {
     company: companyData,
     userId: this.userId
   };
   this.http.registerCompany(requestData).subscribe((registeredCompanyInformation: IResponse) => {
    if (registeredCompanyInformation.status === 'success') {
      this.notifications.success(registeredCompanyInformation.data.message, 'Éxito');
    } else {
      this.notifications.error(registeredCompanyInformation.error.info, 'Error');
    }
   });
  }

  /**
   * @description Set the current istep in documents
   * @return void
   */
  public goToDocuments() {
    this.step++;
  }

  /**
   * @param FormData filesData
   */
  public addFiles(filesData: FormData) {
    filesData.forEach((doc, value) => {
    });
    this.filesToUpdate = filesData;
  }

  /**
   * @description Add the selected image
   * @param FormData image
   */
  public addImage(image: FormData) {
    this.companyLogo = image;
  }

  /**
   * @description Set the selected college
   * @param College college
   */
  public SaveCollege(college: College) {
    this.currentCollege = college;
    this.sumbitAll = true;
  }

  /**
   * @description Save all the data
   * @return void
   */
  public SaveAllData() {
    const requestData = { company: this.GenerateCompanyData() , userId: this.userId, logo: this.companyLogo};
    this.http.registerCompany(requestData)
    .subscribe((companyDataResponse: IResponse) => {
      if (companyDataResponse.status === CONSTANTS.SUCCESS) {
        this.http.registerCompanyLogo(this.companyLogo, companyDataResponse.data.info)
        .subscribe((response: IResponse) => {
          if (response.status === CONSTANTS.SUCCESS) {
            this.http.registerDocuments(this.filesToUpdate, _.head(companyDataResponse.data.info).companyId)
            .subscribe((documentsResponse: IResponse) => {
              if (documentsResponse.status === CONSTANTS.SUCCESS) {
                this.RegisterCollege(this.currentCollege, _.head(companyDataResponse.data.info).companyId );
              } else {
                this.notifications.error(documentsResponse.error.info, 'Error');
              }
            });
        } else {
          this.notifications.error(response.error.info, 'Error');
        }
      });
      } else {
        this.notifications.error(companyDataResponse.error.info, 'Error');
      }
    });
  }

  /**
   * @description Disable the submit all button
   * @return void
   */
  public DisableSubmit() {
    this.sumbitAll = false;
  }

  /**
   * @description Register the company in a college
   * @param College college
   * @param number companyId
   */
  public RegisterCollege(college: College, companyId: number) {
    this.http.joinCompanyCollege(college, companyId).subscribe((collegeCompanyData: IResponse) => {
      if (collegeCompanyData.status === CONSTANTS.SUCCESS) {
        this.notifications.success(collegeCompanyData.data.message, 'Éxito');
        this.appRouter.navigate(['company_data']);
      } else {
        this.notifications.error(collegeCompanyData.error.info, 'Error');
      }
    });
  }

  /**
   * @description Save all the documentation selected by the merchant
   * @param number companyId
   * @returns boolean
   */
  public saveDocuments(companyId: number): boolean {
    let documentsSaved: boolean;
    this.http.registerDocuments(this.filesToUpdate, companyId).subscribe((documentsResponse: IResponse) => {
      if (documentsResponse.status === CONSTANTS.SUCCESS) {
        documentsSaved = true;
      } else {
        this.notifications.error(documentsResponse.error.info, 'Error');
        documentsSaved = false;
      }
    });
    return documentsSaved;
  }

  /**
   * @description Make the upload files button available
   * @param boolean canUpload
   */
  private verifyUpload(canUpload: boolean) {
    this.uploadFiles = true;
  }

  /**
   * @description Upload all the files in the component
   * @return  void
   */
  private makeUpload() {
    this.uploader.uploadAll();
  }

  /**
   * @description Make the upload files button disabled
   * @return  void
   */
  private resetUploadFiles() {
    this.uploadFiles = false;
  }

  /**
   * @description Set the documents headers
   * @return void
   */
  private setDocumentsHeaders() {
    this.documentFiles = [
      { fieldName: LegalDocumentsName.cfid, title: 'CFID' },
      { fieldName: LegalDocumentsName.constitutiveAct, title: 'Acta constitutiva' },
      { fieldName: LegalDocumentsName.ownerId, title: 'Identificación oficial del dueño de la empresa' },
      { fieldName: LegalDocumentsName.proofOfTaxAddress, title: 'Prueba de la dirección físcal' }
    ];

    this.imageNames = [
      { fieldName: 'image', title: 'Logo de la empresa'}
    ];
  }

  /**
   * @description GEnerate the company data that is going to be sent
   * @returns Company
   */
  private GenerateCompanyData(): Company {
    const company: Company = new Company();
    this.TaxDataForm.addControl(TaxDataNames.typeOfTaxpayer, new FormControl(this.taxPlayerType));
    company.CommercialInfo = new CommercialInfo(this.CommercialInfoForm);
    company.BankInformation = new BankInformation(this.BankInfoForm);
    company.TaxData = new TaxData(this.TaxDataForm);
    return company;
  }
}
