import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadFilesComponent } from 'src/app/components/upload-files/upload-files.component';
import { IUploaderData } from 'src/app/interfaces/IComponents';
import { IResponse } from 'src/app/interfaces/IResponse';
import { IProduct, IUser } from 'src/app/interfaces/ITables';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { CONSTANTS } from 'src/app/Utils/Constants';
import { Links } from 'src/app/Utils/Links';
import { productTable } from 'src/app/Utils/TableHeaders';

import * as _ from 'lodash';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ShowDetailsComponent } from 'src/app/components/show-details/show-details.component';
import { AddProductComponent } from 'src/app/components/add-product/add-product.component';
import { TableComponent } from 'src/app/components/table/table.component';
import { Product } from 'src/app/clases/Product';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  @ViewChild('uploader', {static: false}) uploader: UploadFilesComponent;
  @ViewChild('tableA', {static: false}) table: TableComponent;
  private ids: Array<string>;
  private headers: any;
  private products: Array<any>;
  private documentFiles: Array<IUploaderData>;
  private canUpload: boolean;
  private userInfo: IUser;
  private companyId: number;
  private showTable: boolean;
  private timeSpented: number;
  private showProgressBar: boolean;

  constructor(
    private http: HttpServiceService,
    private notifications: NotificationsService,
    private dialog: MatDialog,
    private storage: LocalStorageServiceService
  ) {
    this.ids = productTable.ids;
    this.canUpload = false;
    this.headers = productTable.headers;
    this.products = new Array<IProduct>();
    this.documentFiles = new Array<IUploaderData>();
    this.showTable = false;
    this.timeSpented = CONSTANTS.ZERO;
    this.showProgressBar = false;
   }

  ngOnInit() {
    if (!_.isUndefined(this.http)) {
      this.getProducts();
      this.getOwnedCompany();
      this.setDocumentFiles();
    }
  }

  /**
   * @returns void
   */
  public getProducts(): void {
    this.products = new Array<any>();
    this.showTable = false;
    this.http.getProducts().subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        const currentProducts: Product[] = response.data.info as Product[];
        _.forEach(currentProducts, (product: Product) => {
          this.products.push({ name: product.name, description:  product.description,
            cost: product.cost, amount: product.amount, image: product.image, actions: product
            , edit: product, remove: product
          });
        });
        this.showTable = true;
      }
    });
  }

  /**
   * @returns void
   */
  public getOwnedCompany(): void {
    this.storage.getUserInfo().then((user: IUser) => {
      this.userInfo = user;
      this.http.getCompanyInfo(this.userInfo.id).then((data: IResponse) => {
        if (data.status === CONSTANTS.SUCCESS) {
          this.companyId = _.head(data.data.info).id;
        } else {
          this.notifications.error(data.error.info, 'Error');
        }
      });
    })
    .catch((error) => {
      this.notifications.error(error, 'Error');
    });
  }

  /**
   * @param IProduct product
   * @returns void
   */
  public showProductDetail(product: IProduct): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      detailsType: CONSTANTS.PRODUCT,
      data: product
    };
    this.dialog.open(ShowDetailsComponent, StepperConfig).afterClosed().subscribe(() => {});
  }

   /**
    * @returns void
    */
  private setDocumentFiles(): void {
    this.documentFiles.push({ fieldName: 'products', title: 'Productos'});
  }

   /**
    * @returns void
    */
  private downloadTemplate(): void {
    window.open(Links.productsTemplate);
  }

  /**
   * @param FormData products
   * @returns void
   */
  private registerProducts(products: FormData): void {
    this.showProgressBar = true;
    this.http.uploadMasiveProducts(products, this.companyId).subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        const interval = setInterval(() => {
          this.timeSpented += 100 / _.head(response.data.info);
          if (this.timeSpented === CONSTANTS.ONE_HUNDRED) {
            clearInterval(interval);
            this.timeSpented = CONSTANTS.ZERO;
            this.showProgressBar = false;
            this.notifications.success(response.data.message, 'Éxito');
            this.getProducts();
            this.table.resetTableData(this.products);
          }
        }, CONSTANTS.ONE_SECOND);
      } else {
        this.notifications.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @returns void
   */
  private verifyUpload(): void {
    this.canUpload = true;
  }

  /**
   * @returns void
   */
  private uploadFile(): void {
    this.uploader.uploadAll();
    this.canUpload = false;
  }

  /**
   * @returns void
   */
  private addNewProduct(): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      detailsType: CONSTANTS.PRODUCT,
      companyId: this.companyId,
      isEdit: false
    };
    this.dialog.open(AddProductComponent, StepperConfig).afterClosed().subscribe(() => {
      this.resetProductTable();
    });
  }

  /**
   * @param Product selectedProduct
   * @returns void
   */
  private editProduct(selectedProduct: Product): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      detailsType: CONSTANTS.PRODUCT,
      companyId: this.companyId,
      product: selectedProduct,
      isEdit: true
    };
    this.dialog.open(AddProductComponent, StepperConfig).afterClosed().subscribe(() => {
      this.resetProductTable();
    });
  }

  /**
   * @param Product product
   * @returns void
   */
  private removeProduct(product: Product): void {
     this.http.removeProduct(product)
    .subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        this.notifications.success(response.data.message, 'Éxito');
        this.resetProductTable();
      } else {
        this.notifications.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @returns void
   */
  private resetProductTable(): void {
    this.getProducts();
    setTimeout(() => {
      this.table.resetTableData(this.products);
    }, CONSTANTS.ONE_SECOND);
  }
}
