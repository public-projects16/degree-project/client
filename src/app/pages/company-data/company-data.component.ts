import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { IResponse } from 'src/app/interfaces/IResponse';
import { ICompany, IUser } from 'src/app/interfaces/ITables';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { Router } from '@angular/router';

import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommercialInfo } from 'src/app/clases/CommercialInfo';
import { CONSTANTS, UserStatus } from 'src/app/Utils/Constants';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DisableAccountComponent } from 'src/app/components/disable-account/disable-account.component';

@Component({
  selector: 'app-company-data',
  templateUrl: './company-data.component.html',
  styleUrls: ['./company-data.component.css']
})
export class CompanyDataComponent implements OnInit {

  private companyData: ICompany;
  private userInfo: IUser;
  private CommercialInfoForm: FormGroup;
  private isEdit: boolean;

  constructor(
  private http: HttpServiceService,
  private notification: NotificationsService,
  private router: Router,
  private formBuilder: FormBuilder,
  private storage: LocalStorageServiceService,
  private dialog: MatDialog
  ) {
    this.isEdit = false;
   }

  ngOnInit() {
    this.setCompanyData();
  }

  /**
   * @returns void
   */
  public generateCommercialInfoForm(): void {
    this.CommercialInfoForm = this.formBuilder.group({
      name:  [this.companyData.commercialInfo.name, Validators.required],
      webUrl: [this.companyData.commercialInfo.webUrl, Validators.required],
      contactMail: [this.companyData.commercialInfo.contactMail, Validators.required],
      contactPhone: [this.companyData.commercialInfo.contactPhone, Validators.required],
      socialNetwork: [this.companyData.commercialInfo.socialNetwork, Validators.required],
    });
    this.disableInputs();
  }

  /**
   * @returns void
   */
  public registerCompany() {
    this.router.navigate(['register-company', {userId: this.userInfo.id}]);
  }

  /**
   * @returns void
   */
  public saveChanges() {
    const commercialInfo: CommercialInfo = new CommercialInfo(this.CommercialInfoForm);
    commercialInfo.Id = this.companyData.id;
    this.http.updateCommercialInfo(commercialInfo).subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        this.notification.success(response.data.message, 'Éxito');
        this.disableInputs();
        this.isEdit = false;
      } else {
        this.notification.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @returns void
   */
  public goToDisabled() {
    this.router.navigate(['/disabled/company', { id : this.companyData.id} ]);
  }

  /**
   * @returns void
   */
  private disableInputs() {
    this.CommercialInfoForm.get('name').disable();
    this.CommercialInfoForm.get('webUrl').disable();
    this.CommercialInfoForm.get('contactMail').disable();
    this.CommercialInfoForm.get('contactPhone').disable();
    this.CommercialInfoForm.get('socialNetwork').disable();
  }

  /**
   * @returns void
   */
  private enableInputs() {
    this.CommercialInfoForm.get('webUrl').enable();
    this.CommercialInfoForm.get('contactMail').enable();
    this.CommercialInfoForm.get('contactPhone').enable();
    this.CommercialInfoForm.get('socialNetwork').enable();
  }

  /**
   * @returns void
   */
  private setCompanyData() {
    if (!_.isUndefined(this.storage)) {
      this.storage.getUserInfo().then((user: IUser) => {
        this.userInfo = user;
        return this.http.getCompanyInfo(this.userInfo.id);
      }).then((companyInfo: IResponse) => {
        if (companyInfo.status === 'success') {
          this.companyData = _.head(companyInfo.data.info);
          if (_.isEqual(this.companyData.status, UserStatus.disabled)) {
            this.goToDisabled();
          }
          this.generateCommercialInfoForm();
        } else if (companyInfo.status === 'warn') {
           this.notification.warn(companyInfo.error.info, 'Info');
        } else {
          this.notification.error(companyInfo.error.info, 'Error');
        }
      })
      .catch((error) => {
        this.notification.error(error, 'Error');
      });
    }
  }

  /**
   * @returns void
   */
  private enableEdit() {
    this.isEdit = true;
    this.enableInputs();
  }

  /**
   * @returns void
   */
  private disableCompany() {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      companyId: this.companyData.id,
      action: 'disable',
      type: 'company'
    };
    this.dialog.open(DisableAccountComponent, StepperConfig).afterClosed().subscribe(() => {
      this.setCompanyData();
    });
  }
}
