import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DisableAccountComponent } from 'src/app/components/disable-account/disable-account.component';
import { Links } from 'src/app/Utils/Links';

@Component({
  selector: 'app-disabled-company',
  templateUrl: './disabled-company.component.html',
  styleUrls: ['./disabled-company.component.css']
})
export class DisabledCompanyComponent implements OnInit {

  private links = Links;
  private companyId: number;


  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.companyId = params.id;
    });
  }

  /**
   * @returns void
   */
  public reactivateCompany(): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      companyId: this.companyId,
      action: 'reactivate',
      type: 'company'
    };
    this.dialog.open(DisableAccountComponent, StepperConfig).afterClosed().subscribe(() => {});
  }

}
