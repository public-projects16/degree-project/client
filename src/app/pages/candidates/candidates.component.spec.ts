import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import { CandidatesComponent } from './candidates.component';

describe('CandidatesComponent', () => {
  let component: CandidatesComponent;
  let fixture: ComponentFixture<CandidatesComponent>;
  let http: HttpServiceService;
  let notifications: NotificationsService;
  let storage: LocalStorageServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesComponent ],
      providers: [ 
        { provide: HttpServiceService, useValue: http },
        { provide: NotificationsService, useValue: notifications },
        { provide: LocalStorageServiceService, useValue: storage },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
