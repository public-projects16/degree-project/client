import { Component, OnInit, ViewChild } from '@angular/core';
import { IResponse } from 'src/app/interfaces/IResponse';
import { HttpServiceService } from 'src/app/services/http/http-service.service';

import * as _ from 'lodash';
import { CONSTANTS } from 'src/app/Utils/Constants';
import { IUser } from 'src/app/interfaces/ITables';
import { workersTable } from 'src/app/Utils/TableHeaders';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { TableComponent } from 'src/app/components/table/table.component';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {

  @ViewChild('tableA', {static: false}) table: TableComponent;
  private userInfo: IUser;
  private ids: Array<string>;
  private headers: any;
  private workers: Array<any>;
  private showTable: boolean;

  constructor(private http: HttpServiceService,
              private notifications: NotificationsService,
              private storage: LocalStorageServiceService) {
    this.ids = workersTable.ids;
    this.headers = workersTable.headers;
    this.workers = new Array<any>();
    this.showTable = false;
   }

  ngOnInit() {
    if (!_.isUndefined(this.storage)) {
      this.getUserInformation();
    }
  }

  /**
   * @returns void
   */
  public getUserInformation(): void {
    this.storage.getUserInfo().then((user: IUser) => {
      this.userInfo = user;
      this.getCandidates();
    })
    .catch((error) => {
      this.notifications.error(error, 'Error');
    });
  }

  /**
   * @returns void
   */
  public getCandidates(): void {
    this.http.getCandidatesOnCompany(this.userInfo.id)
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        _.forEach(response.data.info, (worker => {
          this.workers.push({name: worker.name + ' ' + worker.last_name,
          email: worker.email, accept: worker, remove: worker});
        }));
        this.showTable = true;
      }
    });
  }


  /**
   * @param * worker
   * @returns void
   */
  public acceptWorker(worker: any): void {
    this.http.acceptWorker({workerId: worker.id})
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notifications.success(response.data.message, 'Éxito');
        this.deleteWorker(worker);
      } else {
        this.notifications.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @param * worker
   * @returns void
   */
  public declineWorker(worker: any): void {
    this.http.declineWorker({userId: worker.user_id})
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notifications.success(response.data.message, 'Éxito');
        this.deleteWorker(worker);
      } else {
        this.notifications.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @params  worker
   * @returns any
   */
  private deleteWorker(worker: any) {
    const index = _.findIndex(this.workers, {email: worker.email});
    this.workers.splice(index, 1);
    this.table.resetTableData(this.workers);
  }

}
