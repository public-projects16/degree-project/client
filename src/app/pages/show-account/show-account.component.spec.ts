import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import { ShowAccountComponent } from './show-account.component';

describe('ShowAccountComponent', () => {
  let component: ShowAccountComponent;
  let fixture: ComponentFixture<ShowAccountComponent>;
  let httpMock: any;
  let storageMock: any;
  let notificationMock: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [ ShowAccountComponent ],
      providers: [ MatDialog,
        { provide: HttpServiceService, useValue: httpMock},
        { provide: NotificationsService, useValue: notificationMock },
        { provide: LocalStorageServiceService, useValue: storageMock},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
