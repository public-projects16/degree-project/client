import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { IResponse } from 'src/app/interfaces/IResponse';
import { IUserData } from 'src/app/interfaces/IRoles';
import { costumerTable } from 'src/app/Utils/TableHeaders';
import { CONSTANTS, Roles } from 'src/app/Utils/Constants';

import * as _ from 'lodash';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DisableAccountComponent } from 'src/app/components/disable-account/disable-account.component';
import { ICompany } from 'src/app/interfaces/ITables';
import { DateUtils } from 'src/app/Utils/DateUtils';

export interface BuysTable {
  buys: string;
  company: number;
  last: string;
}


@Component({
  selector: 'app-show-account',
  templateUrl: './show-account.component.html',
  styleUrls: ['./show-account.component.css']
})


export class ShowAccountComponent implements OnInit {

  private userData: IUserData;
  private status: Array<string>;
  private companyInfo: ICompany;
  private costumerTabHeaders: Array<string>;
  private headers: any;
  private ids: Array<string>;
  private data: Array<BuysTable> = [];
  private dataClass: string;
  private showBuysTableData = false;

  constructor(
    private http: HttpServiceService,
    private storage: LocalStorageServiceService,
    private notification: NotificationsService,
    private dialog: MatDialog
  ) {
  }

  /**
   * @description Activate when the component init
   * @return void
   */
  public ngOnInit(): void {
    this.setArrays();
    this.setUserInfo();
  }

   /**
    * @description Set the user information
    * @return void
    */
  public setUserInfo(): void {
    if (!_.isUndefined(this.storage)) {
      this.storage.getUserData().then((userData: IUserData) => {
        this.userData = userData;
        if (_.isEqual(this.userData.user.type, Roles.merchant)) {
          this.http.getCompanyInfo(this.userData.user.id)
          .then((companyResponse: IResponse) => {
            if (_.isEqual(companyResponse.status, CONSTANTS.SUCCESS)) {
              this.companyInfo = _.head(companyResponse.data.info);
            }
          });
        } else if (_.isEqual(this.userData.user.type, Roles.costumer)) {
          this.http.getCostumerTransactions(this.userData.user.id).subscribe((response: IResponse) => {
            if (response.status === 'success') {
              this.setTableData(response.data.info);
            } else  { this.notification.error(response.error.info, 'Error'); }
          });
        }
      });
    }
  }

  /**
   * @description Set the status array with the statuses
   * @return void
   */
  public setArrays(): void {
    this.status = ['Disponible', 'Desactivado', 'En espera'];
    this.ids = costumerTable.ids;
    this.headers = costumerTable.headers;
  }

  /**
   * @description Set the clases
   * @return void
   */
  public setClasses(): void {
    if (this.userData.user.type === Roles.costumer) {
      this.dataClass = 'data';
    }
  }

  /**
   * @description Disable the selected account
   * @return void
   */
  public disableAccount(): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      userId: this.userData.user.id,
      action: 'disable',
      type: 'account'
    };
    this.dialog.open(DisableAccountComponent, StepperConfig).afterClosed().subscribe(() => {});
  }

  private setTableData(info: any[]) {
    const uniqueInformation: any[] = [];
    _.forEach(info, (transaction) => {
    const foundTransactionIndex =  _.findIndex(uniqueInformation, (value) => {
        return value.companyId === transaction._company.id;
      });
    if (!_.isEqual(foundTransactionIndex, -CONSTANTS.ONE)) {
      uniqueInformation[foundTransactionIndex].shoppings++;
    } else {
      uniqueInformation.push({
        shoppings: CONSTANTS.ONE,
        companyId: transaction._company.id,
        name: transaction._company.commercialInfo.name,
        date: transaction._date
      });
    }
    });
    _.forEach(uniqueInformation, (row) => {
      this.data.push({
        company: row.name,
        buys: row.shoppings,
        last: DateUtils.formatDate(new Date(row.date))
      });
    });
    this.showBuysTableData = true;
  }
}
