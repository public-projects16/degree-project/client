import { Component, OnInit } from '@angular/core';
import { IResponse } from 'src/app/interfaces/IResponse';
import { IUser } from 'src/app/interfaces/ITables';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import * as _ from 'lodash';
import { companyTransactions } from 'src/app/Utils/TableHeaders';
import { DateUtils } from 'src/app/Utils/DateUtils';

@Component({
  selector: 'app-company-transactions',
  templateUrl: './company-transactions.component.html',
  styleUrls: ['./company-transactions.component.css']
})
export class CompanyTransactionsComponent implements OnInit {

  private userData: IUser;
  private ids: Array<string>;
  private headers: any;
  private tableData: Array<any>;
  private showTable: boolean;
  private companyId: number;

  constructor(
    private http: HttpServiceService,
    private notification: NotificationsService,
    private storage: LocalStorageServiceService
  ) {
    this.tableData = [];
   }

  ngOnInit() {
    if (!_.isUndefined(this.storage)) {
      this.storage.getUserInfo().then((user: IUser) => {
        this.userData = user;
        this.http.getCompanyInfo(this.userData.id).then((response: IResponse) => {
          if (_.isEqual(response.status, 'success')) {
            this.companyId = _.head(response.data.info).id;
            this.setCompanyTransactions();
          }
        })
        .catch((error) => {
          this.notification.error(error, 'Error');
        });
      })
      .catch((error) => {
        this.notification.error(error, 'Error');
      });
    }
  }

  public setCompanyTransactions() {
    this.http.findCompanyTransactions(this.companyId).subscribe((response: IResponse) => {
      if (_.isEqual(response.status, 'success')) {
        this.setTableDataInfo(response.data.info);
      } else {
        this.notification.error(response.error.info, 'Error');
      }
    });
  }

  private setTableDataInfo(info: any[]) {
    _.forEach(info, (row) => {
      this.tableData.push({
        concept: row._concept,
        amount: row._shoppingTotal,
        date: DateUtils.formatDate(new Date(row._date))
      });
    });
    this.ids = companyTransactions.ids;
    this.headers = companyTransactions.headers;
    this.showTable = true;
  }
}
