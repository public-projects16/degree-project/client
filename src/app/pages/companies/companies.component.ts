import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { IResponse } from 'src/app/interfaces/IResponse';
import { ICollege } from 'src/app/interfaces/IRoles';
import { ICommercialInfo } from 'src/app/interfaces/ITables';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { CONSTANTS } from 'src/app/Utils/Constants';

import * as _ from 'lodash';
import { companiesOnCollegesTable } from 'src/app/Utils/TableHeaders';
import { TableComponent } from 'src/app/components/table/table.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ShowDetailsComponent } from 'src/app/components/show-details/show-details.component';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit, AfterViewChecked {

  @ViewChild('tableA', {static: false}) table: TableComponent;
  private dataInfo: Array<ICommercialInfo>;
  private ids: Array<string>;
  private headers: any;

  constructor(
    private http: HttpServiceService,
    private notification: NotificationsService,
    private changes: ChangeDetectorRef,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  /**
   * @returns void
   */
  public ngAfterViewChecked(): void {
    this.changes.detectChanges();
  }

  /**
   * @param ICollege college
   * @returns void
   */
  public SearchCompaniesOnCollege(college: ICollege): void {
    this.http.getCompanyByCollegeId(college.id).subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        this.setDataInfo(response.data.info);
      } else {
        this.notification.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @param ICommercialInfo[] companies
   * @returns CompaniesComponent
   */
  public setDataInfo(companies: ICommercialInfo[]): void {
    const currentDataInfo = new Array<any>();
    _.forEach(companies, (company) => {
      currentDataInfo.push({
        company: company.name,
        actions: company
      });
    });
    this.headers = companiesOnCollegesTable.headers;
    this.ids = companiesOnCollegesTable.ids;
    this.dataInfo = currentDataInfo;
    if ( !_.isUndefined(this.table)) {
      this.table.resetTableData(currentDataInfo);
    }
  }

  /**
   * @param ICommercialInfo commercialInfo
   * @returns void
   */
  public showCommercialInfoDetails(commercialInfo: ICommercialInfo): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      detailsType: CONSTANTS.COMMERCIAL_INFO,
      data: commercialInfo
    };
    this.dialog.open(ShowDetailsComponent, StepperConfig).afterClosed().subscribe(() => {});
  }
}
