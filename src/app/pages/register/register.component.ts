import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Roles } from 'src/app/Utils/Constants';
import { ICostumerRegister, IWorkerRegister, IMerchantRegister } from 'src/app/interfaces/IRegisters';
import { Links } from 'src/app/Utils/Links';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from '../../services/notifications/notification.service';
import { IResponse } from 'src/app/interfaces/IResponse';
import { MsalService } from 'src/app/services/msal/msal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public generalDataForm: FormGroup;
  public workerDataForm: FormGroup;
  public merchantDataForm: FormGroup;
  public costumerDataForm: FormGroup;
  public userRol: string =  Roles.costumer;
  public roles = Roles;
  public links = Links;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpServiceService,
    private notify: NotificationsService,
    private msal: MsalService,
    private router: Router) { }

  public ngOnInit() {
    this.generalDataForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      birthday: ['', Validators.required],
    });

    this.costumerDataForm = this.formBuilder.group({
      nip: ['', Validators.required]
    });

    this.workerDataForm = this.formBuilder.group({
      key: ['', Validators.required]
    });
    this.merchantDataForm = this.formBuilder.group({
      curp: ['', Validators.required]
    });
  }

  /**
   * @description Register the costumer
   * @return void
   */
  public  registerCostumer(): void {
    const costumerObject: ICostumerRegister = this.generateCostumerObject();
    this.http.registerCostumer(costumerObject).subscribe((data: IResponse) => {
      if (data.status === 'success') {
        this.notify.success(data.data.message, 'Éxito');
        this.router.navigate(['/costumer']);
      } else {
        this.notify.error(data.error.info, 'Error');
      }
    });
  }

  /**
   * @description Register the current worker
   * @return RegisterComponent
   */
  public registerWorker(): void {
    const workerObject: IWorkerRegister = this.generateWorkerObject();
    this.http.registerWorker(workerObject).subscribe( (data: IResponse) => {
      if (data.status === 'success') {
        this.notify.success(data.data.message, 'Éxito');
        this.router.navigate(['/worker']);
      } else {
        this.notify.error(data.error.info, 'Error');
      }
    });
  }

  /**
   * @description Register the merchant
   * @return void
   */
  public registerMerchant(): void {
    const merchantObject: IMerchantRegister = this.generateMerchantObject();
    this.http.registerMerchant(merchantObject).subscribe( (data: IResponse) => {
      if (data.status === 'success') {
        this.notify.success(data.data.message, 'Éxito');
        this.router.navigate(['/merchant']);
      } else {
        this.notify.error(data.error.info, 'Error');
      }
    });
  }

  /**
   * @description Close the session in Azure
   * @return void
   */
  private logout(): void {
    this.msal.logout();
  }

  /**
   * @description Generate the costumer object
   * @returns ICostumerRegister
   */
  private generateCostumerObject(): ICostumerRegister {
    return {
      user : {
        name: ( this.generalDataForm.value.name ) ? this.generalDataForm.value.name : '',
        lastname: ( this.generalDataForm.value.lastName ) ? this.generalDataForm.value.lastName : '',
        birthday: ( this.generalDataForm.value.birthday ) ? this.generalDataForm.value.birthday : ''
      },
      nip:  ( this.costumerDataForm.value.nip ) ? this.costumerDataForm.value.nip : ''
    };
  }

  /**
   * @description Generate the worker object
   * @returns IWorkerRegister
   */
  private generateWorkerObject(): IWorkerRegister {
    return {
      user : {
        name: this.generalDataForm.value.name,
        lastname: this.generalDataForm.value.lastName,
        birthday: this.generalDataForm.value.birthday
      },
      a_access_key: this.workerDataForm.value.key
    };
  }

  /**
   * @description Generate the merchant object
   * @returns IMerchantRegister
   */
  private generateMerchantObject(): IMerchantRegister {
    return {
      user : {
        name: this.generalDataForm.value.name,
        lastname: this.generalDataForm.value.lastName,
        birthday: this.generalDataForm.value.birthday
      },
      curp: this.merchantDataForm.value.curp
    };
  }
}
