import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { MsalService } from 'src/app/services/msal/msal.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let httpMock: HttpServiceService;
  let notifyMock: NotificationsService;
  let msalMock: MsalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ RegisterComponent ],
      providers: [FormBuilder,
        { provide: HttpServiceService, useValue: httpMock},
        { provide: NotificationsService, useValue: notifyMock },
        { provide: MsalService, useValue: msalMock}
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
