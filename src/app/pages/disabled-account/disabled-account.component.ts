import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/interfaces/ITables';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';
import { Links } from 'src/app/Utils/Links';

import { MatDialog, MatDialogConfig } from '@angular/material';
import { DisableAccountComponent } from 'src/app/components/disable-account/disable-account.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-disabled-account',
  templateUrl: './disabled-account.component.html',
  styleUrls: ['./disabled-account.component.css']
})
export class DisabledAccountComponent implements OnInit {

  private links: any;
  private user: IUser;

  constructor(
    private storage: LocalStorageServiceService,
    private notificator: NotificationsService,
    private http: HttpServiceService,
    private dialog: MatDialog
  ) {
    this.links = Links;
  }

  public ngOnInit() {
    if (!_.isUndefined(this.storage)) {
      this.storage.getUserInfo()
      .then((user: IUser) => {
        this.user = user;
      })
      .catch((error) => {
        this.notificator.error(error, 'Error');
      });
    }
  }

  /**
   * @returns void
   */
  public reactivateAccount(): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      userId: this.user.id,
      action: 'reactivate',
      type: 'account'
    };
    this.dialog.open(DisableAccountComponent, StepperConfig).afterClosed().subscribe(() => {});
  }
}
