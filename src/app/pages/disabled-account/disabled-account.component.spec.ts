import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import { DisabledAccountComponent } from './disabled-account.component';

describe('DisabledAccountComponent', () => {
  let component: DisabledAccountComponent;
  let fixture: ComponentFixture<DisabledAccountComponent>;
  let storageMock: LocalStorageServiceService;
  let notificatorMock: NotificationsService;
  let httpMock: HttpServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [ DisabledAccountComponent ],
      providers: [MatDialog, 
        { provide: NotificationsService, useValue: notificatorMock},
        { provide: HttpServiceService, useValue: httpMock},
        { provide: LocalStorageServiceService, useValue: storageMock},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisabledAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
