import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

import { ProductsToSellComponent } from './products-to-sell.component';

describe('ProductsToSellComponent', () => {
  let component: ProductsToSellComponent;
  let fixture: ComponentFixture<ProductsToSellComponent>;
  let http: HttpServiceService;
  let notificator: NotificationsService;
  let storage: LocalStorageServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatDialogModule],
      declarations: [ ProductsToSellComponent ],
      providers: [ MatDialog, 
        { provide: HttpServiceService, useValue: http },
        { provide: NotificationsService, useValue: notificator },
        { provide: LocalStorageServiceService, useValue: storage },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsToSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
