import { Component, OnInit } from '@angular/core';
import { IResponse } from 'src/app/interfaces/IResponse';
import { HttpServiceService } from 'src/app/services/http/http-service.service';

import * as _ from 'lodash';

import { CONSTANTS } from 'src/app/Utils/Constants';
import { ICompany, IProduct, IUser } from 'src/app/interfaces/ITables';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { Links } from 'src/app/Utils/Links';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ShoppingPreviewComponent } from 'src/app/components/shopping-preview/shopping-preview.component';
import { LocalStorageServiceService } from 'src/app/services/storage/local-storage-service.service';

@Component({
  selector: 'app-products-to-sell',
  templateUrl: './products-to-sell.component.html',
  styleUrls: ['./products-to-sell.component.css']
})
export class ProductsToSellComponent implements OnInit {

  private user: IUser;
  private company: ICompany;
  private products: IProduct[];
  private isActive: boolean;
  private links: any;
  private selectedProduct: IProduct;
  private productsInShoppingCart: IProduct[];

  constructor(
    private http: HttpServiceService,
    private notificator: NotificationsService,
    private dialog: MatDialog,
    private storage: LocalStorageServiceService
    ) {
      this.isActive = false;
      this.links = Links;
      this.productsInShoppingCart = new Array<IProduct>();
     }

  ngOnInit() {
    if (!_.isUndefined(this.storage)) {
      this.setInformation();
    }
  }

  /**
   * @description Set the user information
   * @returns void
   */
  public setInformation(): void {
    this.storage.getUserInfo().then((user: IUser) => {
        this.user = user;
        this.setCompanyInfo();
    })
    .catch((error) => {
      this.notificator.error(error, 'Error');
    });
  }

  /**
   * @description set the company information
   * @return  void
   */
  public setCompanyInfo(): void {
    this.http.getCompanyByWorker(this.user.id).subscribe((companyResponse: IResponse) => {
      if (_.isEqual(companyResponse.status, CONSTANTS.SUCCESS)) {
        this.company = _.head(companyResponse.data.info);
        if (!_.isEqual(this.company.id, CONSTANTS.INITIAL_ID)) {
          this.isActive = true;
          this.setProductsInfo();
        }
      } else {
        this.notificator.error(companyResponse.error.info, 'Error');
      }
    });
  }

  /**
   * @description Set the products information
   * @returns void
   */
  public setProductsInfo(): void {
    this.http.getProductsToSell(this.company.id).subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.products = response.data.info;
        console.log(this.products);
        this.selectedProduct = _.head(this.products);
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Search the especified product
   * @param string text
   * @returns void
   */
  public searchProduct(text: string): void {
    const productIndex = _.findIndex(this.products, (value: any) => {
      return value._name.toLowerCase().includes(text.toLowerCase());
    });
    if (!_.isEqual(productIndex, -CONSTANTS.ONE)) {
      this.selectedProduct = this.products[productIndex];
    }
  }

  /**
   * @description Add the selected product to the shopping cart
   * @param IProduct product
   * @returns void
   */
  public addProductToCart(product: IProduct): void {
    const clonedProduct = _.clone(product);
    let productIndex = _.findIndex(this.productsInShoppingCart, (value: any) => {
      return value._id === product._id;
    });
    if (_.isEqual(productIndex, -CONSTANTS.ONE)) {
      clonedProduct._amount = CONSTANTS.ONE;
      this.productsInShoppingCart.push(clonedProduct);
      productIndex = _.findIndex(this.products, (value) => {
          return value._id === product._id;
        });
      this.products[productIndex]._amount--;
      this.notificator.info('Producto agregado', '');
    } else {
      if (!this.verifyProductAmount(product)) {
        this.productsInShoppingCart[productIndex]._amount++;
        productIndex = _.findIndex(this.products, (value) => {
          return value._id === product._id;
        });
        this.products[productIndex]._amount--;
        this.notificator.info('Producto agregado', '');
      } else {
        this.notificator.error('No cuenta con más producto en el inventario', 'Error');
      }
    }
  }

  /**
   * @param IProduct product
   * @returns boolean
   */
  public verifyProductAmount(product: IProduct): boolean {
    const productIndex = _.findIndex(this.products, (value) => {
      return value._id === product._id;
    });
    return _.isEqual(this.products[productIndex]._amount, CONSTANTS.ZERO);
  }

  /**
   * @param IProduct product
   * @returns void
   */
  public deleteProductOnCart(product: IProduct): void {
    let productIndex = _.findIndex(this.productsInShoppingCart, (value) => {
      return value._id === product._id;
    });
    if (_.isEqual(productIndex, -CONSTANTS.ONE)) {
      this.notificator.error('El producto seleccionado no se encuentra en el carrito', 'Error');
    } else {
      if (_.isEqual(this.productsInShoppingCart[productIndex]._amount, CONSTANTS.ONE)) {
        this.productsInShoppingCart.splice(productIndex, CONSTANTS.ONE);
      } else {
        this.productsInShoppingCart[productIndex]._amount--;
      }
      productIndex = _.findIndex(this.products, (value) => {
        return value._id === product._id;
      });
      this.products[productIndex]._amount++;
      this.notificator.info('Producto eliminado', '');
    }
  }

  /**
   * @description Show a new mat dialog with the cart information
   * @returns void
   */
  public showCart(): void {
    if (this.productsInShoppingCart.length > CONSTANTS.ZERO) {
      const StepperConfig = new MatDialogConfig();
      StepperConfig.disableClose = true;
      StepperConfig.autoFocus = true;
      StepperConfig.width = '60%';
      StepperConfig.data = {
        isShoppingFinished: false,
        products: this.productsInShoppingCart,
      };
      this.dialog.open(ShoppingPreviewComponent, StepperConfig).afterClosed().subscribe(() => {
      });
    } else {
      this.notificator.error('No cuenta con productos en el carrito.', 'Error');
    }
  }

  /**
   * @description Show a new mat dialog with the cart information
   * @returns void
   */
  public showShoppingPreview(): void {
    const StepperConfig = new MatDialogConfig();
    StepperConfig.disableClose = true;
    StepperConfig.autoFocus = true;
    StepperConfig.width = '60%';
    StepperConfig.data = {
      isShoppingFinished: true,
      products: this.productsInShoppingCart,
      company: this.company
    };
    this.dialog.open(ShoppingPreviewComponent, StepperConfig).afterClosed().subscribe(() => {
      this.productsInShoppingCart = new Array<IProduct>();
    });
  }
}
