export interface IUser {
    id?: number;
    name: string;
    last_name: string;
    email: string;
    birthday: string;
    type: string;
    status: number;
}

export interface IWorker {
    id?: number;
    b_access_key?: string;
    a_access_key?: string;
    company_id?: number;
    user: IUser;
}

export interface ICostumer {
    id?: number;
    user: IUser;
    NIP: string;
}

export interface IMerchant {
    id?: number;
    user: IUser;
    a_access_key?: string;
    CURP: string;
}

export interface ITransaction {
    id?: number;
    costumerId?: number;
    companyId: number;
    amount: number;
    concept: string;
    date: Date;
}

export interface ICommercialInfo {
    id?: number;
    name: string;
    webUrl: string;
    contactMail: string;
    contactPhone: string;
    socialNetwork: string;
    logo?: string;
}

export interface ITaxData {
    id?: number;
    phone: string;
    taxRfc: string;
    taxAddress: string;
}

export interface ILegalDocumentation {
    id?: number;
    constitutiveAct: string;
    CFID: string;
    ownerId: string;
    proofOfTaxAddress: string;
}

export interface ICompany {
    id?: number;
    commercialBusiness: string;
    typeOfTaxpayer: string;
    commercialInfo?: ICommercialInfo;
    taxData?: ITaxData;
    status?: number;
    legalDocumentation?: ILegalDocumentation;
}

export interface IProduct {
    _id?: number;
    actions?: IProduct;
    _edit?: IProduct;
    _remove?: IProduct;
    _name: string;
    _cost: number;
    _image: string;
    _description: string;
    _amount: number;
}

export interface IBankInformation {
    id?: number;
    secret_key: string;
    clabe: string;
}

export interface ICompany {
    id?: number;
    user_id?: number;
    commercial_business: string;
    type_of_taxpayer: string;
    commercial_info?: ICommercialInfo;
    tax_data?: ITaxData;
    legal_documentation?: ILegalDocumentation;
    bank_information?: IBankInformation;
}

export interface IStatus {
    id?: number;
    description: string;
}
