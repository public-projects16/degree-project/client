export class IUserRegister {
    name: string;
    lastname: string;
    birthday: string;
}

export interface ICostumerRegister {
    user: IUserRegister;
    nip: string;
}

export interface IWorkerRegister {
    user: IUserRegister;
    a_access_key: string;
}

export interface IMerchantRegister {
    user: IUserRegister;
    curp: string;
}
