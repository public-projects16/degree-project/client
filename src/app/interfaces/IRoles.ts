interface IUser {
    id?: number;
    name: string;
    lastname: string;
    birthday: string;
    email: string;
    type: string;
}


export interface ICostumer {
    user: IUser;
    NIP?: string;
}

export interface IMerchant {
    user: IUser;
    a_access_key?: string;
    CURP: string;
}

export interface IWorker {
    user: IUser;
    b_access_key?: string;
}

export interface ICollege {
    id?: number;
    email: string;
    institution: string;
    campus: string;
    logo?: string;
}

export interface IUserData {
    user: IUser;
    college?: ICollege;
}
