export interface IUploaderData {
    fieldName: string;
    title: string;
}
