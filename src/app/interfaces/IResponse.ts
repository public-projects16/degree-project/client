interface IDataResponse {
    info: Array<any>;
    code?: number;
    message: string;
}

interface IErrorResponse {
    info: string;
    code?: number;
}

export interface IResponse {
    status: string;
    data?: IDataResponse;
    error?: IErrorResponse;
}
