import { MsalGuard } from '@azure/msal-angular';
import { CanActivate, Router } from '@angular/router';
import { environment } from '../environments/environment';
import { LogLevel } from 'msal';
import { Injectable } from '@angular/core';
import { MsalService } from './services/msal/msal.service';

import * as _ from 'lodash';
import { HttpServiceService } from './services/http/http-service.service';
import { IResponse } from './interfaces/IResponse';
import { CONSTANTS, UserStatus } from './Utils/Constants';
import { IUser } from './interfaces/ITables';

export function loggerCallback(logLevel: LogLevel.Verbose, message, piiEnabled: true) {
}

@Injectable()

export class AuthGuard implements CanActivate {
  private tokenKey: string;
  private user: string;

  constructor(private msal: MsalGuard,
              private msalService: MsalService,
              private router: Router,
              private http: HttpServiceService
              ) {
    this.tokenKey = environment.tokenKey;
    this.user = 'user';
  }

  async canActivate(route: any, state: any) {
    this.msal.canActivate(route, state);
    const token = sessionStorage.getItem(this.tokenKey);
    this.validateUser();
    if ( !_.isNull(token)) {
        localStorage.setItem(this.tokenKey, token);
        sessionStorage.clear();
    }
    return true;
  }

  private validateUser(): void {
    if (_.isNull(localStorage.getItem(this.user))) {
      this.http.getUserInfo().then((response: IResponse) => {
        if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
          localStorage.setItem(this.user, JSON.stringify(_.head(response.data.info)));
        }
      })
      .catch((error: any) => {
      });
    } else {
        const user: IUser = JSON.parse(localStorage.getItem(this.user)).user;
        if (_.isEqual(user.status, UserStatus.disabled)) {
          this.router.navigate(['/disabled']);
        }
    }
  }
}
