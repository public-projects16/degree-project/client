import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material';

import { ShoppingPreviewComponent } from './shopping-preview.component';

describe('ShoppingPreviewComponent', () => {
  let component: ShoppingPreviewComponent;
  let fixture: ComponentFixture<ShoppingPreviewComponent>;
  let matDialogDataMock: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [ ShoppingPreviewComponent ],
      providers: [ MatDialog, { provide: MAT_DIALOG_DATA, useValue: matDialogDataMock}],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingPreviewComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
