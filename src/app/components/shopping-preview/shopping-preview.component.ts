import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { IProduct } from 'src/app/interfaces/ITables';
import { productsToSell } from 'src/app/Utils/TableHeaders';

import * as _ from 'lodash';

@Component({
  selector: 'app-shopping-preview',
  templateUrl: './shopping-preview.component.html',
  styleUrls: ['./shopping-preview.component.css']
})
export class ShoppingPreviewComponent implements OnInit {

  private ids: Array<string>;
  private headers: any;
  private data: any;
  private gridData: Array<any>;
  private step: number;
  private elementType: string;
  private correctionLevel: string;
  private value: any;
  private qrData: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public injectedData: any,
    private dialog: MatDialog
  ) {
    this.ids = productsToSell.ids;
    this.headers = productsToSell.headers;
    this.gridData = new Array<any>();
    this.step = 0;
   }

  /**
   * @description Calls the main functions when the component starts
   * @returns void
   */
  public ngOnInit(): void {
    if (!_.isUndefined(this.injectedData)) {
      this.data = this.injectedData;
      this.setGridData();
    }
  }

  /**
   * @description Set the grid definition
   * @returns void
   */
  private setGridData(): void {
    _.forEach(this.data.products, (product: IProduct) => {
      this.gridData.push({
        name: product._name, price: product._cost, amount: product._amount, subtotal: product._amount * product._cost
      });
    });
  }

  /**
   * @description Set the qr data
   * @returns void
   */
  private showQr(): void {
    const total: number = _.sumBy(this.gridData, 'subtotal');
    const qrProducts = [];
    _.forEach(this.data.products, (product: IProduct) => {
      qrProducts.push({
        name: product._name,
        id: product._id,
        amount: product._amount,
        cost: product._cost
      });
    });
    const json = {
      companyId: this.data.company.id,
      shoppingTotal: total,
      products: qrProducts
    };
    this.qrData = JSON.stringify(json);
    this.step++;
  }

  /**
   * @description Close the current dialog
   * @returns void
   */
  private close(): void {
    this.dialog.closeAll();
  }

}
