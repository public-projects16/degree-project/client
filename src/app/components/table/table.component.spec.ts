import { ChangeDetectorRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTableModule } from '@angular/material';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let notification: NotificationsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatTableModule],
      declarations: [ TableComponent ],
      providers: [ ChangeDetectorRef,
        { provide: NotificationsService, useValue: notification }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
