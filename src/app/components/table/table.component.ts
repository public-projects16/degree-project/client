import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, AfterViewChecked, Output, EventEmitter } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit, AfterViewChecked {

  @Input() type: string;
  @Input() headers: any;
  @Input() ids: Array<string>;
  @Input() data: Array<string>;
  @Output() private emiiter: EventEmitter<any>;
  @Output() private editEmiiter: EventEmitter<any>;
  @Output() private removeEmiiter: EventEmitter<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  private displayedColumns: Array<string>;
  private dataSource: MatTableDataSource<any>;
  private isEmpty: boolean;

  constructor(
    private notification: NotificationsService,
    private cdrf: ChangeDetectorRef
  ) {
      this.emiiter = new EventEmitter<any>();
      this.editEmiiter = new EventEmitter<any>();
      this.removeEmiiter = new EventEmitter<any>();
   }

  /**
   * @retuns void
   */
  public ngOnInit() {
    this.verifyDataNoEmpty();
    this.configurateTable();
  }

  /**
   * @returns void
   */
  public ngAfterViewChecked(): void {
    this.cdrf.detectChanges();
  }

  /**
   * @param Event event
   * @returns void
   */
  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * @returns void
   */
  public configurateTable(): void {
    this.displayedColumns = this.ids;
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator;
  }

  /**
   * @returns void
   */
  public verifyDataNoEmpty(): void {
    if ( this.data.length === 0 ) {
      this.notification.info('No se encontraron datos', '');
    }
  }

  /**
   * @param Array<T> data
   * @returns void
   */
  public resetTableData<T>(data: Array<T>): void {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
  }

  /**
   * @param T value
   * @returns void
   */
  public emitCurrentObject<T>(value: T ): void {
    this.emiiter.emit(value);
  }

  /**
   * @param T value
   * @returns void
   */
  public emitEditObject<T>(value: T): void {
    this.editEmiiter.emit(value);
  }

  /**
   * @param T value
   * @returns void
   */
  public emitRemoveObject<T>(value: T): void {
    this.removeEmiiter.emit(value);
  }
}
