import * as _ from 'lodash';

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Product } from 'src/app/clases/Product';
import { IResponse } from 'src/app/interfaces/IResponse';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';
import { CONSTANTS } from 'src/app/Utils/Constants';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  private productForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public injectedData: any,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private http: HttpServiceService,
    private notificator: NotificationsService
  ) { }

  ngOnInit() {
    if (!_.isUndefined(this.injectedData)) {
      this.generateProductForm();
    }
  }

  /**
   * @description Generate the product form
   * @returns void
   */
  public generateProductForm(): void {
    this.productForm = this.formBuilder.group({
      name: [ !_.isUndefined(this.injectedData.product) ? this.injectedData.product.name : '', Validators.required],
      cost: [!_.isUndefined(this.injectedData.product) ? this.injectedData.product.cost : '' , Validators.required],
      image: [!_.isUndefined(this.injectedData.product) ? this.injectedData.product.image : '' , Validators.required],
      amount: [!_.isUndefined(this.injectedData.product) ? this.injectedData.product.amount : '' , Validators.required],
      description: [!_.isUndefined(this.injectedData.product) ? this.injectedData.product.description : '', Validators.required]
    });
  }

  /**
   * @description Add a new product in the cart
   * @returns void
   */
  private AddProduct() {
    const newProduct: Product = new Product(this.productForm);
    this.http.addProduct(newProduct, this.injectedData.companyId)
    .subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        this.notificator.success(response.data.message, 'Éxito');
        this.dialog.closeAll();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Edit the selected product
   * @returns void
   */
  private EditProduct() {
    const productEdited: Product = new Product(this.productForm);
    productEdited.id = this.injectedData.product.id;
    this.http.editProduct(productEdited)
    .subscribe((response: IResponse) => {
      if (response.status === CONSTANTS.SUCCESS) {
        this.notificator.success(response.data.message, 'Éxito');
        this.dialog.closeAll();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description close the dialog
   * @returns void
   */
  private closeDialog() {
    this.dialog.closeAll();
  }

}
