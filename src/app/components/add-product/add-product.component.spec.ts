import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import { AddProductComponent } from './add-product.component';

describe('AddProductComponent', () => {
  let component: AddProductComponent;
  let fixture: ComponentFixture<AddProductComponent>;
  let http: HttpServiceService;
  let notificator: NotificationsService;
  let madDialogData: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatDialogModule],
      declarations: [ AddProductComponent ],
      providers: [ FormBuilder, MatDialog,
        { provide: HttpServiceService, useValue: http },
        { provide: NotificationsService, useValue: notificator},
        { provide: MAT_DIALOG_DATA, useValue: madDialogData}
       ],
       schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProductComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
