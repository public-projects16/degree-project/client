import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import * as _ from 'lodash';

@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.css']
})
export class ShowDetailsComponent implements OnInit {

  private data: any;
  private detailsType: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public injectedData: any,
    private dialog: MatDialog
  ) { }

  /**
   * @returns void
   */
  public ngOnInit() {
    if (!_.isUndefined(this.injectedData)) {
      this.data = this.injectedData.data;
      this.detailsType = this.injectedData.detailsType;
    }
  }

  /**
   * @returns void
   */
  private close() {
    this.dialog.closeAll();
  }
}
