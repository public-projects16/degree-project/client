import { Component, OnInit, EventEmitter, Input, Output, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import * as _ from 'lodash';

import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { IEvent } from '../../interfaces/IEvents';
import { IUploaderData } from 'src/app/interfaces/IComponents';
import { CONSTANTS } from 'src/app/Utils/Constants';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.css']
})

export class UploadFilesComponent implements OnInit, AfterViewChecked {

  @Input() private fields: Array<IUploaderData>;
  @Input() private allowedExt: Array<string>;
  @Output() private filesEmiter: EventEmitter<FormData>;
  @Output() private uploadEmmiter: EventEmitter<boolean>;

  private attachmentList: any = {};
  private showButton: boolean;

  constructor(
    private http: HttpServiceService,
    private notifications: NotificationsService,
    private cdrf: ChangeDetectorRef
    ) {
    this.filesEmiter = new EventEmitter<FormData>();
    this.uploadEmmiter = new EventEmitter<boolean>();
    this.showButton = false;
  }

  ngOnInit() {}

  /**
   * @returns void
   */
  public ngAfterViewChecked(): void {
    this.cdrf.detectChanges();
  }

  /**
   * @param IEvent<HTMLInputElement> event
   * @param string field
   * @returns void
   */
  public saveFile(event: IEvent<HTMLInputElement>, field: string): void {
    if (this.allowedExt.includes(_.head(event.target.files).name.split('.').pop() )) {
      this.attachmentList[field] = _.head(event.target.files);
      this.verifyAllFilesUpdated();
    } else {
      this.notifications.error('Solo se permiten archivos con extensión ' + this.allowedExt.toString(), 'Error');
    }
  }

  /**
   * @returns void
   */
  public uploadAll() {
    const formData: FormData = new FormData();
    _.forEach(this.fields, (field: IUploaderData) => {
      formData.append(field.fieldName , this.attachmentList[field.fieldName]);
    });
    this.filesEmiter.emit(formData);
  }

  /**
   * @returns void
   */
  public verifyAllFilesUpdated() {
    let fileUploaded: number = CONSTANTS.ZERO;
    _.forEach(this.fields, (field: IUploaderData) => {
      if (!_.isUndefined(this.attachmentList[field.fieldName])) {
        fileUploaded++;
      }
    });
    if (_.isEqual(fileUploaded, this.fields.length)) {
      this.uploadAll();
      this.uploadEmmiter.emit(true);
    }
  }

}
