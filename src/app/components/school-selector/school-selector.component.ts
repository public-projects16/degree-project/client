import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { College } from 'src/app/clases/College';
import { IResponse } from 'src/app/interfaces/IResponse';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-school-selector',
  templateUrl: './school-selector.component.html',
  styleUrls: ['./school-selector.component.css']
})
export class SchoolSelectorComponent implements OnInit {

  private institutes: College[];
  private colleges: College[];
  private selectedInstitution: string;
  private selectCampus: boolean;
  private campusSelcted: College;

  @Output() schoolEmitter: EventEmitter<College>;
  @Output() institutionEmitter: EventEmitter<string>;

  constructor(
    private http: HttpServiceService,
    private notification: NotificationsService
  ) {
    this.institutes = [];
    this.selectCampus = false;
    this.schoolEmitter = new EventEmitter<College>();
    this.institutionEmitter = new EventEmitter<string>();
    this.campusSelcted = new College();
   }

  /**
   * @returns void
   */
  public ngOnInit() {
    if (!_.isUndefined(this.http) ) {
      this.http.getInstitutes().subscribe((response: IResponse) => {
        if (response.status === 'success') {
          this.institutes = response.data.info;
        } else {
          this.notification.error(response.error.info, 'Error');
        }
      });
    }
  }

  /**
   * @returns void
   */
  public SelectCampus() {
    this.http.getCampus(this.selectedInstitution).subscribe((data: IResponse) => {
      if (data.status === 'success') {
        this.colleges = data.data.info;
        this.selectCampus = true;
        this.institutionEmitter.emit(this.selectedInstitution);
      } else {
        this.notification.error(data.error.info, 'Error');
      }
    });
  }

  /**
   * @returns void
   */
  public SendCollege() {
    this.schoolEmitter.emit(this.campusSelcted);
  }

}
