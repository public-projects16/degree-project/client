import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import { SchoolSelectorComponent } from './school-selector.component';

describe('SchoolSelectorComponent', () => {
  let component: SchoolSelectorComponent;
  let fixture: ComponentFixture<SchoolSelectorComponent>;
  let http: HttpServiceService;
  let notification: NotificationsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolSelectorComponent ],
      providers: [
        { provide: HttpServiceService, useValue: http },
        { provide: NotificationsService, useValue: notification },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
