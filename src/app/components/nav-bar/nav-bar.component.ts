import { Component, Input, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MsalService } from 'src/app/services/msal/msal.service';
import { Links } from 'src/app/Utils/Links';
import { Roles } from 'src/app/Utils/Constants';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  @Input() public type: string;

  public links = Links;

  public fillerNav: Array<any>;

  public isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  constructor(
    private readonly router: Router,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly msal: MsalService,
  ) {}

  ngOnInit() {
     this.setNavOptions();
  }

  /**
   * @description Set t5he nav options according the role
   * @returns void
   */
  private setNavOptions(): void {
    if (this.type !== undefined) {
      if (this.type === Roles.costumer) { this.setCostumerOptions();
      } else if (this.type === Roles.worker) { this.setWorkerOptions();
      } else { this.setMerchantOptions(); }
    }
  }

  /**
   * @description set the costumer nav bar options
   * @returns void
   */
  private setCostumerOptions(): void {
    this.fillerNav = [
      { name: 'Inicio', route: '', icon: 'home' },
      { name: 'Transacciones', route: 'transactions', icon: 'credit_card' },
      { name: 'Empresas', route: 'companies', icon: 'store' },
      { name: 'Cuenta', route: 'account', icon: 'account_circle' },
    ];
  }

  /**
   * @description set the worker nav bar options
   * @returns void
   */
  private setWorkerOptions(): void {
    this.fillerNav = [
      { name: 'Inicio', route: '', icon: 'home' },
      { name: 'Cuenta', route: 'account', icon: 'account_circle' },
      { name: 'Catálogo', route: 'companyProducts', icon: 'book' },
    ];
  }

  /**
   * @description set the merchant nav bar options
   * @returns void
   */
  private setMerchantOptions(): void {
    this.fillerNav = [
      { name: 'Inicio', route: '', icon: 'home' },
      { name: 'Cuenta', route: 'account', icon: 'account_circle' },
      { name: 'Empresa', route: 'company_data', icon: 'store' },
      { name: 'Transacciones', route: 'company/transactions', icon: 'credit_card' },
      { name: 'Trabajadores', route: 'candidates', icon: 'face' },
      { name: 'Productos', route: 'products', icon: 'book' },
    ];
  }

  /**
   * @description Navigate to the specified route
   * @param string route
   * @returns void
   */
  private selectRoute(route: string): void {
    this.router.navigate(['/' + route]);
  }

  /**
   * @description Close the current session in Azure
   * @returns void
   */
  private logOut(): void {
    localStorage.clear();
    sessionStorage.clear();
    this.msal.logout();
  }


}
