import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { MsalService } from 'src/app/services/msal/msal.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import { DisableAccountComponent } from './disable-account.component';

describe('DisableAccountComponent', () => {
  let component: DisableAccountComponent;
  let fixture: ComponentFixture<DisableAccountComponent>;
  let httpMock: any;
  let notifyMock: any;
  let msalMock: any;
  let matDialogDataMock: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatDialogModule],
      declarations: [ DisableAccountComponent ],
      providers: [MatDialog,
        { provide: HttpServiceService, useValue: httpMock},
        { provide: NotificationsService, useValue: notifyMock },
        { provide: MsalService, useValue: msalMock},
        { provide: MAT_DIALOG_DATA, useValue: matDialogDataMock}
      ],
        schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisableAccountComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
