import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { IResponse } from 'src/app/interfaces/IResponse';
import { HttpServiceService } from 'src/app/services/http/http-service.service';
import { NotificationsService } from 'src/app/services/notifications/notification.service';

import * as _ from 'lodash';
import { CONSTANTS } from 'src/app/Utils/Constants';
import { MsalService } from 'src/app/services/msal/msal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-disable-account',
  templateUrl: './disable-account.component.html',
  styleUrls: ['./disable-account.component.css']
})
export class DisableAccountComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public injectedData: any,
    private dialog: MatDialog,
    private http: HttpServiceService,
    private notificator: NotificationsService,
    private msal: MsalService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  /**
   * @description 
   * @returns void
   */
  private disableUser(): void {
    this.http.disableAccount(this.injectedData.userId)
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notificator.success(response.data.message, 'Éxito');
        this.msal.logout();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Reactivate a user
   * @returns void
   */
  private reactivateUser(): void {
    this.http.reactivateAccount(this.injectedData.userId)
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notificator.success(response.data.message, 'Éxito');
        this.msal.logout();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Disable a company
   * @returns void
   */
  private disableCompany(): void {
    this.http.disableCompany(this.injectedData.companyId)
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notificator.success(response.data.message, 'Éxito');
        this.closeDialog();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Reactivate a company
   * @returns void
   */
  private reactivateCompany(): void {
    this.http.reactivateCompany(this.injectedData.companyId)
    .subscribe((response: IResponse) => {
      if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
        this.notificator.success(response.data.message, 'Éxito');
        this.router.navigate(['/company_data']);
        this.closeDialog();
      } else {
        this.notificator.error(response.error.info, 'Error');
      }
    });
  }

  /**
   * @description Close the current dialog
   * @returns void
   */
  private closeDialog(): void {
    this.dialog.closeAll();
  }
}
