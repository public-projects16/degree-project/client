import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ICostumerRegister, IWorkerRegister, IMerchantRegister } from 'src/app/interfaces/IRegisters';
import { College } from 'src/app/clases/College';
import { CommercialInfo } from 'src/app/clases/CommercialInfo';
import { Product } from 'src/app/clases/Product';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private http: HttpClient) { }

  /**
   * @returns Observable<any>
   */
  public getTokenInformation(): Observable<any> {
    return   this.http.get(environment.API + '/access');
  }

  /**
   * @param ICostumerRegister costumer
   * @returns Observable<any>
   */
  public  registerCostumer(costumer: ICostumerRegister): Observable<any> {
    return  this.http.post(environment.API + '/costumer/register', costumer);
  }

  /**
   * @param IWorkerRegister worker
   * @returns Observable<any>
   */
  public  registerWorker(worker: IWorkerRegister): Observable<any> {
    return  this.http.post(environment.API + '/worker/register', worker);
  }

  /**
   * @param IMerchantRegister merchant
   * @returns Observable<any>
   */
  public  registerMerchant(merchant: IMerchantRegister): Observable<any> {
    return  this.http.post(environment.API + '/merchant/register', merchant);
  }

  /**
   * @returns Observable<any>
   */
  public validateUserRegistered(): Observable<any> {
    return this.http.get(environment.API + '/costumer/validate');
  }

  /**
   * @returns Promise<any>
   */
  public getUserInfo(): Promise<any> {
    return this.http.get(environment.API + '/user').toPromise();
  }

  /**
   * @param number id
   * @returns Observable<any>
   */
  public getCostumerTransactions(id: number): Observable<any> {
    return this.http.get(environment.API + '/costumer/transactions/' + id);
  }

  /**
   * @param number userId
   * @returns Promise<any>
   */
  public getCompanyInfo(userId: number): Promise<any> {
    return this.http.get(environment.API + '/company/info/' + userId).toPromise();
  }

  /**
   * @param * requestData
   * @returns Observable<any>
   */
  public registerCompany(requestData: any): Observable<any> {
    console.log(requestData);
    return this.http.post(environment.API + '/company/register', requestData);
  }

  /**
   * @param FormData documents
   * @param number companyId
   * @returns Observable<any>
   */
  public registerDocuments(documents: FormData, companyId: number): Observable<any> {
    documents.forEach((doc, value) => {
    });
    return this.http.post(environment.API + '/company/registerDocuments/' + companyId , documents);
  }

  /**
   * @param College college
   * @param number companyId
   * @returns Observable<any>
   */
  public joinCompanyCollege(college: College, companyId: number): Observable<any> {
    return this.http.post(environment.API + '/company/joinSchool/' + companyId, college);
  }

  /**
   * @returns Observable<any>
   */
  public getInstitutes(): Observable<any> {
    return this.http.get(environment.API + '/college/show/names');
  }

  /**
   * @param string institute
   * @returns Observable<any>
   */
  public getCampus(institute: string): Observable<any> {
    return this.http.get(environment.API + '/college/campus/' + institute);
  }

  /**
   * @param CommercialInfo commercialInfo
   * @returns Observable<any>
   */
  public updateCommercialInfo(commercialInfo: CommercialInfo): Observable<any> {
    return this.http.put(environment.API + '/company/update/commercialInfo', commercialInfo);
  }

  /**
   * @param number collegeId
   * @returns Observable<any>
   */
  public getCompanyByCollegeId(collegeId: number): Observable<any> {
    return this.http.get(environment.API + '/company/infoByCollege/' + collegeId);
  }

  /**
   * @returns Observable<any>
   */
  public getProducts(): Observable<any> {
    return this.http.get(environment.API + '/products/show/');
  }

  /**
   * @param FormData products
   * @param number companyId
   * @returns Observable<any>
   */
  public uploadMasiveProducts(products: FormData, companyId: number): Observable<any> {
    return this.http.post(environment.API + '/products/masiveUpload/' + companyId , products);
  }

  /**
   * @param Product product
   * @param number companyId
   * @returns Observable<any>
   */
  public addProduct(product: Product, companyId: number): Observable<any> {
    return this.http.post(environment.API + '/products/add/' + companyId, product.toJSON());
  }

  /**
   * @param Product product
   * @returns Observable<any>
   */
  public removeProduct(product: Product): Observable<any> {
    return this.http.post(environment.API + '/products/remove', product);
  }

  /**
   * @param Product product
   * @returns Observable<any>
   */
  public editProduct(product: Product): Observable<any> {
    return this.http.post(environment.API + '/products/edit', product);
  }

  /**
   * @param FormData logo
   * @param * ids
   * @returns Observable<any>
   */
  public registerCompanyLogo(logo: FormData, ids: any): Observable<any> {
    const commercialInfoId = _.head<any>(ids).commercialInfoId;
    const companyId =  _.head<any>(ids).companyId;
    return this.http.post(environment.API + '/company/registerLogo/' + commercialInfoId + '/' + companyId , logo );
  }

  /**
   * @param number userId
   * @returns Observable<any>
   */
  public getCandidatesOnCompany(userId: number): Observable<any> {
    return this.http.get(environment.API + '/company/showProspects/' + userId);
  }

  /**
   * @param object body
   * @returns Observable<any>
   */
  public acceptWorker(body: object): Observable<any> {
    return this.http.post(environment.API + '/company/acceptWorker', body);
  }

  /**
   * @param object body
   * @returns Observable<any>
   */
  public declineWorker(body: object): Observable<any> {
    return this.http.post(environment.API + '/company/declineWorker', body);
  }

  /**
   * @param number userId
   * @returns Observable<any>
   */
  public getCompanyByWorker(userId: number): Observable<any> {
    return this.http.get(environment.API + '/company/workerOnCompany/' + userId);
  }

  /**
   * @param number companyId
   * @returns Observable<any>
   */
  public getProductsToSell(companyId: number): Observable<any> {
    return this.http.get(environment.API + '/products/productToSell/' + companyId);
  }

  /**
   * @param number UserId
   * @returns Observable<any>
   */
  public disableAccount(UserId: number): Observable<any> {
    return this.http.put(environment.API + '/user/disable', {userId: UserId});
  }

  /**
   * @param number UserId
   * @returns Observable<any>
   */
  public reactivateAccount(UserId: number): Observable<any> {
    return this.http.put(environment.API + '/user/reactivate', {userId: UserId});
  }

  /**
   * @param number CompanyId
   * @returns Observable<any>
   */
  public disableCompany(CompanyId: number): Observable<any> {
    return this.http.put(environment.API + '/company/disable', {companyId: CompanyId});
  }

  /**
   * @param number CompanyId
   * @returns Observable<any>
   */
  public reactivateCompany(CompanyId: number): Observable<any> {
    return this.http.put(environment.API + '/company/reactivate', {companyId: CompanyId});
  }

  /**
   * @param number companyId
   * @returns Observable<any>
   */
  public findCompanyTransactions(companyId: number): Observable<any> {
    return this.http.get(environment.API + '/transactions/company/transact/' + companyId);
  }
}
