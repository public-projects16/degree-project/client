import { Injectable } from '@angular/core';
import * as Msal from 'msal';
import { environment } from 'src/environments/environment';
import msalConfiguration from 'src/app/Utils/MsalConfiguration';

@Injectable({
  providedIn: 'root',
})
export class MsalService {

  public token: string;
  private clientApplication: Msal.UserAgentApplication;

  constructor() {
    this.setClientApplication();
  }

  /**
   * @returns string The acces token sent by azure
   */
  public getToken(): string {
    const token = localStorage.getItem(environment.tokenKey);
    return token;
  }

  /**
   * @description Close the session in Azure
   * @return void
   */
  public logout(): void {
    localStorage.removeItem(environment.tokenKey);
    localStorage.clear();
    sessionStorage.clear();
    this.clientApplication.logout();
  }

  /**
   * @return void
   */
  private setClientApplication() {
    this.clientApplication = new Msal.UserAgentApplication( msalConfiguration.getConfiguration());
  }
}
