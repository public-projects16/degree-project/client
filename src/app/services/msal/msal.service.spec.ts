import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { MsalService } from './msal.service';

describe('MsalService', () => {
  let service: MsalService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ MsalService ]
    });
  });

  beforeEach(() => {
    service = TestBed.get(MsalService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(httpMock).toBeTruthy();
  });
});
