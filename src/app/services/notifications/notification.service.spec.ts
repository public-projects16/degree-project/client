import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ToastPackage, ToastRef, ToastrModule, ToastrService, TOAST_CONFIG } from 'ngx-toastr';

import { NotificationsService } from './notification.service';

describe('NotificationService', () => {
  let toastConfigMock: any;
  let toastPackageMock: {
    toastId: number;
    toastType: string;
    afterActivate: jasmine.Spy;
    config: { toastClass: string; default: string; };
    message: string;
    default: any;
    title: string;
    toastRef: ToastRef<unknown>;
};
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ToastrModule.forRoot()],
    providers: [ { provide: ToastPackage, useValue: toastPackageMock }],
    schemas: [NO_ERRORS_SCHEMA]
  }));

  it('should be created', () => {
    const service: NotificationsService = TestBed.get(NotificationsService);
    expect(service).toBeTruthy();
  });
});
