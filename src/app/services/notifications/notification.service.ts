import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class NotificationsService {

  constructor(private toastr: ToastrService) { }

  /**
   * @param string msg The message is going to be showed
   * @param string title The message title
   */
  public success(msg: string, title: string) {
    this.toastr.success(msg, title, {
      enableHtml: true,
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-top-right',
      timeOut: 5000
    });
  }

  /**
   * @param string msg The message is going to be showed
   * @param string title The message title
   */
  public error(msg: string, title: string) {
    this.toastr.error(msg, title, {
      enableHtml: true,
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-top-right',
      timeOut: 5000
    });
  }

  /**
   * @param string msg The message is going to be showed
   * @param string title The message title
   */
  public warn(msg: string, title: string) {
    this.toastr.warning(msg, title, {
      enableHtml: true,
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-top-right',
      timeOut: 5000
    });
  }

  /**
   * @param string msg The message is going to be showed
   * @param string title The message title
   */
  public info(msg: string, title: string): void {
    this.toastr.info(msg, title, {
      enableHtml: true,
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-top-right',
      timeOut: 5000
    });
  }
}
