import { Injectable } from '@angular/core';
import { IResponse } from 'src/app/interfaces/IResponse';
import { CONSTANTS } from 'src/app/Utils/Constants';
import { HttpServiceService } from '../http/http-service.service';

import * as _ from 'lodash';
import { IUserData } from 'src/app/interfaces/IRoles';
import { IUser } from 'src/app/interfaces/ITables';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageServiceService {

  private user: string;
  private userData: string;

    constructor(
        private http: HttpServiceService,
    ) {
        this.user = 'user';
        this.userData = 'userData';
    }

    /**
     * @returns Promise<IUser> The user that is required
     */
    public getUserInfo(): Promise<IUser> {
        let promise: Promise<IUser>;
        promise = new Promise((resolve, reject) => {
            if (!_.isNull(localStorage.getItem(this.user))) {
                resolve(JSON.parse(localStorage.getItem(this.user)).user);
               } else {
                   this.http.getUserInfo().then((response: IResponse) => {
                       if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
                         localStorage.setItem(this.user, JSON.stringify(_.head(response.data.info)));
                         resolve(JSON.parse(localStorage.getItem(this.user)).user);
                       } else {
                          reject(response.error.info);
                       }
                   });
               }
        });
        return promise;
    }

    /**
     * @returns Promise<IUserData> The user data related eith the acces token
     */
    public getUserData(): Promise<IUserData> {
        let promise: Promise<IUserData>;
        promise = new Promise((resolve, reject) => {
            if (!_.isNull(localStorage.getItem(this.userData))) {
                resolve(JSON.parse(localStorage.getItem(this.user)));
               } else {
                   this.http.getUserInfo().then((response: IResponse) => {
                       if (_.isEqual(response.status, CONSTANTS.SUCCESS)) {
                         localStorage.setItem(this.userData, JSON.stringify(_.head(response.data.info)));
                         resolve(JSON.parse(localStorage.getItem(this.userData)));
                       } else {
                          reject(response.error.info);
                       }
                   });
               }
        });
        return promise;
    }
}
