import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { HttpServiceService } from '../http/http-service.service';

import { LocalStorageServiceService } from './local-storage-service.service';

describe('LocalStorageServiceService', () => {
  let httpMock: any;
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ { provide: HttpServiceService, useValue: httpMock } ],
    schemas: [NO_ERRORS_SCHEMA]
  }));

  it('should be created', () => {
    const service: LocalStorageServiceService = TestBed.get(LocalStorageServiceService);
    expect(service).toBeTruthy();
  });
});
