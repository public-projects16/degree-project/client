import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MsalModule, MsalGuard } from '@azure/msal-angular';
import { AuthGuard } from './authGuard';
import { HomeComponent } from './home/home.component';
import { MsalInterceptor } from './msal.interceptor';
import msalConfiguration from './Utils/MsalConfiguration';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MsalService } from './services/msal/msal.service';
import { RegisterComponent } from './pages/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../Modules/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CostumerComponent } from './pages/costumer/costumer.component';
import { WorkerComponent } from './pages/worker/worker.component';
import { MerchantComponent } from './pages/merchant/merchant.component';
import { ShowAccountComponent } from './pages/show-account/show-account.component';
import { TableComponent } from './components/table/table.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { CompanyDataComponent } from './pages/company-data/company-data.component';
import { RegisterCompanyComponent } from './pages/register-company/register-company.component';
import { UploadFilesComponent } from './components/upload-files/upload-files.component';
import {FileUploadModule} from 'ng2-file-upload';
import { SchoolSelectorComponent } from './components/school-selector/school-selector.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { ShowDetailsComponent } from './components/show-details/show-details.component';
import { ProductsComponent } from './pages/products/products.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { CandidatesComponent } from './pages/candidates/candidates.component';
import { ProductsToSellComponent } from './pages/products-to-sell/products-to-sell.component';
import { ShoppingPreviewComponent } from './components/shopping-preview/shopping-preview.component';
import { QRCodeModule } from 'angularx-qrcode';
import { DisabledAccountComponent } from './pages/disabled-account/disabled-account.component';
import { DisableAccountComponent } from './components/disable-account/disable-account.component';
import { DisabledCompanyComponent } from './pages/disabled-company/disabled-company.component';
import { CompanyTransactionsComponent } from './pages/company-transactions/company-transactions.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    NavBarComponent,
    CostumerComponent,
    WorkerComponent,
    MerchantComponent,
    ShowAccountComponent,
    TableComponent,
    TransactionsComponent,
    CompanyDataComponent,
    RegisterCompanyComponent,
    UploadFilesComponent,
    SchoolSelectorComponent,
    CompaniesComponent,
    ShowDetailsComponent,
    ProductsComponent,
    AddProductComponent,
    CandidatesComponent,
    ProductsToSellComponent,
    ShoppingPreviewComponent,
    DisabledAccountComponent,
    DisableAccountComponent,
    DisabledCompanyComponent,
    CompanyTransactionsComponent,
  ],
  imports: [
    FormsModule,
    QRCodeModule,
    ReactiveFormsModule,
    MaterialModule,
    FileUploadModule,
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MsalModule.forRoot(msalConfiguration.getConfiguration(), {
      consentScopes: [
      'openid',
    ]})
  ],
  providers: [
    MsalGuard,
    AuthGuard,
    MsalService ,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ShowDetailsComponent, AddProductComponent, ShoppingPreviewComponent, DisableAccountComponent ]
})
export class AppModule { }
