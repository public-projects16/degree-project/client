import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { MsalService } from './services/msal/msal.service';

@Injectable()
export class MsalInterceptor implements HttpInterceptor {

    constructor(private msalService: MsalService) { }
    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken = this.msalService.getToken() || '';
        console.log(authToken);
        req = req.clone({
            headers: new HttpHeaders({
                Authorization : 'Bearer ' + authToken
            })
        });
        return next.handle(req);
    }
}
