import * as _ from 'lodash';
import { CONSTANTS } from '../Utils/Constants';
import { Company } from './company';
import { Costumer } from './Costumer';
import { Product } from './Product';

export class Transaction {

    private Company: Company;
    private Costumer: Costumer;
    private Products: Array<Product>;
    private Concept: string;
    private ShoppingTotal: number;
    private Id: number;
    private Date: Date;

    constructor() {
        this.Company = new Company();
        this.Costumer = new Costumer();
        this.Products = new Array<Product>();
        this.Concept = '';
        this.ShoppingTotal = CONSTANTS.ZERO;
        this.Id = CONSTANTS.INITIAL_ID;
        this.Date = new Date();
    }

    get id(): number { return this.Id; }

    set id(id: number) { this.Id = id; }

    get company(): Company { return this.Company; }

    set company(company: Company) { this.Company = company; }

    get costumer(): Costumer { return this.Costumer; }

    set costumer(costumer: Costumer) { this.Costumer = costumer; }

    get concept(): string { return this.Concept; }

    set concept(concept: string) { this.Concept = concept; }

    get shoppingTotal(): number { return this.ShoppingTotal; }

    set shoppingTotal(total: number) { this.ShoppingTotal = total; }

    get date(): Date { return this.Date; }

    set date(date: Date) { this.Date = date; }

    get products(): Array<Product> { return this.Products; }

    set products(products: Product[]) {
        _.forEach(products, (product) => {
            const newProduct: Product = new Product();
            newProduct.name = !_.isUndefined(product.name) ? product.name : '';
            newProduct.amount = !_.isUndefined(product.amount) ? product.amount : CONSTANTS.ZERO;
            newProduct.cost = !_.isUndefined(product.cost) ? product.cost : CONSTANTS.ZERO;
            newProduct.id = !_.isUndefined(product.id) ? product.id : CONSTANTS.INITIAL_ID;
            this.Products.push(newProduct);
        });
    }
}
