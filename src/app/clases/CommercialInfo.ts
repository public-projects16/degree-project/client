import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CommercialInfoNames, CONSTANTS } from '../Utils/Constants';

export class CommercialInfo {
    private id: number;
    private name: string;
    private webUrl: string;
    private contactMail: string;
    private contactPhone: string;
    private socialNetwork: string;
    private commercialBusiness: string;
    private logo: string;

    constructor(form?: FormGroup) {
        if (!_.isUndefined(form)) {
            this.setFormInformationValues(form);
        } else {
            this.setEmptyValues();
        }
    }

    get Id(): number { return this.id; }

    set Id(id: number) { this.id = id; }

    get Name(): string { return this.name; }

    set Name(name: string) { this.name = name; }

    get WebUrl(): string { return this.webUrl; }

    set WebUrl(url: string) { this.webUrl = url; }

    get ContactMail(): string { return this.contactMail; }

    set ContactMail(mail: string) { this.contactMail = mail; }

    get ContactPhone(): string { return this.contactPhone ; }

    set ContactPhone(phone: string) { this.contactPhone = phone; }

    get SocialNetwork(): string { return this.socialNetwork; }

    set SocialNetwork(socialNetwork: string) { this.socialNetwork = socialNetwork; }

    get CommercialBusiness() { return this.commercialBusiness; }

    set CommercialBusiness(value: string) { this.commercialBusiness = value; }

    get Logo() { return this.logo; }

    set Logo(value: string) { this.logo = value; }

    private setEmptyValues(): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.name = '';
        this.webUrl = '';
        this.contactMail = '';
        this.contactPhone = '';
        this.socialNetwork = '';
        this.commercialBusiness = '';
        this.logo = '';
    }

    private setFormInformationValues(form: FormGroup) {
        this.id = CONSTANTS.INITIAL_ID;
        this.name = form.get(CommercialInfoNames.name).value;
        this.webUrl = form.get(CommercialInfoNames.webUrl).value;
        this.contactMail = form.get(CommercialInfoNames.contactMail).value;
        this.contactPhone = form.get(CommercialInfoNames.contactPhone).value;
        this.socialNetwork = form.get(CommercialInfoNames.socialNetwork).value;
        this.commercialBusiness = (form.get(CommercialInfoNames.commercialBusiness) ) ?
        form.get(CommercialInfoNames.commercialBusiness).value : '';
    }
}
