import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CONSTANTS, LegalDocumentsName } from '../Utils/Constants';


export class LegalDocumentation {

    private id: number;
    private constitutiveAct: string;
    private cfid: string;
    private ownerId: string;
    private proofOfTaxAddress: string;

    constructor(form?: FormGroup) {
        if (!_.isUndefined(form)) {
            this.setFormValues(form);
        } else {
            this.setEmptyValues();
        }
    }

    get Id(): number { return this.id; }

    set Id(id: number) { this.id = id; }

    get ConstitutiveAct(): string { return this.constitutiveAct; }

    set ConstitutiveAct(constitutiveAct: string) { this.constitutiveAct = constitutiveAct; }

    get CFID(): string { return this.cfid; }

    set CFID(cfid: string) { this.cfid = cfid; }

    get OwnerId(): string { return this.ownerId; }

    set OwnerId(ownerId: string) { this.ownerId = ownerId; }

    get ProofOfTaxAddress(): string { return this.proofOfTaxAddress ; }

    set ProofOfTaxAddress(proof: string) { this.proofOfTaxAddress = proof; }

    private setEmptyValues(): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.constitutiveAct = '';
        this.cfid = '';
        this.ownerId = '';
        this.proofOfTaxAddress = '';
    }

    private setFormValues(form: FormGroup): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.constitutiveAct = form.get(LegalDocumentsName.constitutiveAct).value;
        this.cfid = form.get(LegalDocumentsName.cfid).value;
        this.ownerId = form.get(LegalDocumentsName.ownerId).value;
        this.proofOfTaxAddress = form.get(LegalDocumentsName.proofOfTaxAddress).value;
    }
}
