import { CONSTANTS } from '../Utils/Constants';
import { User } from './User';

export class Costumer extends User {

    private CostumerId: number;

    constructor() {
        super();
        this.CostumerId = CONSTANTS.INITIAL_ID;
    }

    get costumerId(): number { return this.CostumerId; }

    set costumerId(value: number) { this.CostumerId = value; }
}
