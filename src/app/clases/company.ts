import { CommercialInfo } from './CommercialInfo';
import { TaxData } from './TaxData';
import { LegalDocumentation } from './LegalDocumentation';
import { BankInformation } from './BankInformation';
import { CONSTANTS } from '../Utils/Constants';

export class Company {

    private id: number;
    private commercialInfo: CommercialInfo;
    private taxData: TaxData;
    private legalDocumentation: LegalDocumentation;
    private bankInformation: BankInformation;

    constructor() {
        this.id = CONSTANTS.INITIAL_ID;
        this.commercialInfo = new CommercialInfo();
        this.taxData = new TaxData();
        this.legalDocumentation = new LegalDocumentation();
        this.bankInformation = new BankInformation();
    }

    get Id() { return this.id; }

    set Id(value: number) { this.id = value; }

    get CommercialInfo() { return this.commercialInfo; }

    set CommercialInfo(value: CommercialInfo) { this.commercialInfo = value; }

    get TaxData() { return this.taxData; }

    set TaxData(value: TaxData) { this.taxData = value; }

    get LegalDocumentation() { return this.legalDocumentation; }

    set LegalDocumentation(value: LegalDocumentation) { this.legalDocumentation = value; }

    get BankInformation() { return this.bankInformation; }

    set BankInformation(value: BankInformation) { this.bankInformation = value; }
}
