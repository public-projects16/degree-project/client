import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CONSTANTS, BankInformationNames } from '../Utils/Constants';


export class BankInformation {

    private id: number;
    private secretKey: string;
    private openpayId: string;
    private clabe: string;

    constructor(formInformation?: FormGroup) {
        if (!_.isUndefined(formInformation)) {
            this.setFormValues(formInformation);
        } else {
            this.setEmptyValues();
        }
    }

    get Id(): number { return this.id; }

    set Id(id: number) { this.id = id; }

    get SecreyKey(): string { return this.secretKey; }

    set SecreyKey(secretKey: string) { this.secretKey = secretKey; }

    get CLABE(): string { return this.clabe; }

    set CLABE(clabe: string) { this.clabe = clabe; }

    get OpenPayId(): string { return this.openpayId; }

    set OpenPayId(value: string) { this.openpayId = value; }

    private setEmptyValues(): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.secretKey = '';
        this.clabe = '';
        this.openpayId = '';
    }

    private setFormValues(form: FormGroup): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.secretKey = form.get(BankInformationNames.secretKey).value;
        this.clabe = form.get(BankInformationNames.clabe).value;
        this.openpayId = form.get(BankInformationNames.openpayId).value;
    }
}
