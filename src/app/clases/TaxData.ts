import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';

import { CONSTANTS, TaxDataNames } from '../Utils/Constants';


export class TaxData {

    private id: number;
    private phone: string;
    private taxRfc: string;
    private taxAddress: string;
    private typeOfTaxpayer: string;

    constructor(form?: FormGroup) {
        if (!_.isUndefined(form)) {
            this.setFormInformation(form);
        } else {
            this.setEmptyValues();
        }
    }

    get Id(): number { return this.id; }

    set Id(id: number) { this.id = id; }

    get Phone(): string { return this.phone; }

    set Phone(phone: string) { this.phone = phone; }

    get TaxRfc(): string { return this.taxRfc; }

    set TaxRfc(taxRfc: string) { this.taxRfc = taxRfc; }

    get TaxAddress(): string { return this.taxAddress; }

    set TaxAddress(address: string) { this.taxAddress = address; }

    get TypeOfTaxpayer() { return this.typeOfTaxpayer; }

    set TypeOfTaxpayer(value: string) { this.typeOfTaxpayer = value; }

    private setEmptyValues(): void {
        this.id = CONSTANTS.INITIAL_ID;
        this.phone = '';
        this.taxAddress = '';
        this.taxRfc = '';
        this.typeOfTaxpayer = '';
    }

    private setFormInformation(form: FormGroup) {
        this.id = CONSTANTS.INITIAL_ID;
        this.phone = form.get(TaxDataNames.phone).value;
        this.taxAddress = form.get(TaxDataNames.taxAddress).value;
        this.taxRfc = form.get(TaxDataNames.taxRfc).value;
        this.typeOfTaxpayer = form.get(TaxDataNames.typeOfTaxpayer).value;
    }
}
