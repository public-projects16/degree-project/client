import * as _ from 'lodash';

import { FormGroup } from '@angular/forms';
import { ProductsNames } from '../Utils/Constants';

export class Product  {

    private Name: string;
    private Cost: number;
    private Image: string;
    private Description: string;
    private Amount: number;
    private Id: number;

    constructor(form?: FormGroup) {
        if (!_.isUndefined(form)) {
            this.setData(form);
        }
    }

    get id() { return this.Id; }

    set id(value: number) { this.Id = value; }

    get name() { return this.Name; }

    set name(value: string) { this.Name = value; }

    get cost() { return this.Cost; }

    set cost(value: number) { this.Cost = value; }

    get image() { return this.Image; }

    set image(value: string) { this.Image = value; }

    get description() { return this.Description; }

    set description(value: string) { this.Description = value; }

    get amount() { return this.Amount; }

    set amount(value: number) { this.Amount = value; }

    public toJSON(): object {
        return {
            name: this.Name,
            cost: this.Cost,
            image: this.Image,
            description: this.Description,
            amount: this.Amount
        };
    }
    private setData(form: FormGroup) {
        this.name = form.get(ProductsNames.name).value;
        this.cost = form.get(ProductsNames.cost).value;
        this.image = form.get(ProductsNames.image).value;
        this.description = form.get(ProductsNames.description).value;
        this.amount = form.get(ProductsNames.amount).value;
    }
}
