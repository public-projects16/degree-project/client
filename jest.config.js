module.exports = {
    preset: "jest-preset-angular",
    setupFilesAfterEnv: [
      "<rootDir>/jestConf.ts"
    ],
    transform: {
      "^.+\\.(ts|js|html)$": "ts-jest"
    },
    testRegex: "(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$",
    testPathIgnorePatterns: [
      "<rootDir>/node_modules/",
      "<rootDir>/dist/"
    ]
  };
  